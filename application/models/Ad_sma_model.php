<?php
/* 
 * MRI Indonesia
 * Ari Padrian (www.aripadrian.space)
 */
 
class Ad_sma_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get ad_sma by id
     */
    function get_ad_sma($id)
    {
        return $this->db->get_where('ad_smas',array('id'=>$id))->row_array();
    }
    function get_all() {
        $this->db->order_by('tanggal_arsip', 'desc');
        return $this->db->get('ad_smas')->result_array();
    }
    /*
     * Get all ad_smas
     */
    function get_all_ad_sma($limit, $start)
    {
        $query = $this->db->get('ad_smas', $limit, $start);
        return $query;
    }
        
    /*
     * function to add new ad_sma
     */
    function add_ad_sma($params)
    {
        $this->db->insert('ad_smas',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update ad_sma
     */
    function update_ad_sma($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('ad_smas',$params);
    }
    
    /*
     * function to delete ad_sma
     */
    function delete_ad_sma($id)
    {
        return $this->db->delete('ad_smas',array('id'=>$id));
    }
}
