<?php
/* 
 * MRI Indonesia
 * Ari Padrian (www.aripadrian.space)
 */
 
class Riwayat_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get ad_dp by id
     */
    function get_Riwayat($id)
    {
        return $this->db->get_where('tabel_log',array('id'=>$id))->row_array();
    }
        
    /*
     * Get all Riwayat
     */
    function get_all_Riwayat($limit, $start)
    {
        $this->db->order_by('log_time', 'desc');
        $query = $this->db->get('tabel_log', $limit, $start);
        return $query;
    }
    
    function get_all() {
        $this->db->order_by('log_time', 'desc');
        return $this->db->get('tabel_log')->result_array();
    }
}
