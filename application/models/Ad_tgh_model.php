<?php
/* 
 * MRI Indonesia
 * Ari Padrian (www.aripadrian.space)
 */
 
class Ad_tgh_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get ad_tgh by id
     */
    function get_ad_tgh($id)
    {
        return $this->db->get_where('ad_tgh',array('id'=>$id))->row_array();
    }
    function get_all() {
        $this->db->order_by('tanggal_arsip', 'desc');
        return $this->db->get('ad_tgh')->result_array();
    } 
    /*
     * Get all ad_tgh
     */
    function get_all_ad_tgh($limit, $start)
    {
        $query = $this->db->get('ad_tgh', $limit, $start);
        return $query;
    }
    /*
     * function to add new ad_tgh
     */
    function add_ad_tgh($params)
    {
        $this->db->insert('ad_tgh',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update ad_tgh
     */
    function update_ad_tgh($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('ad_tgh',$params);
    }
    
    /*
     * function to delete ad_tgh
     */
    function delete_ad_tgh($id)
    {
        return $this->db->delete('ad_tgh',array('id'=>$id));
    }
}
