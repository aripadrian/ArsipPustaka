<?php
/* 
 * MRI Indonesia
 * Ari Padrian (www.aripadrian.space)
 */
 
class Ad_sk_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get ad_sk by id
     */
    function get_ad_sk($id)
    {
        return $this->db->get_where('ad_sk',array('id'=>$id))->row_array();
    }
    function get_all() {
        $this->db->order_by('tanggal_arsip', 'desc');
        return $this->db->get('ad_sk')->result_array();
    }
    /*
     * Get all ad_sk
     */
    function get_all_ad_sk($limit, $start)
    {
        $query = $this->db->get('ad_sk', $limit, $start);
        return $query;
    }
        
    /*
     * function to add new ad_sk
     */
    function add_ad_sk($params)
    {
        $this->db->insert('ad_sk',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update ad_sk
     */
    function update_ad_sk($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('ad_sk',$params);
    }
    
    /*
     * function to delete ad_sk
     */
    function delete_ad_sk($id)
    {
        return $this->db->delete('ad_sk',array('id'=>$id));
    }
}
