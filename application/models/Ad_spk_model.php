<?php
/* 
 * MRI Indonesia
 * Ari Padrian (www.aripadrian.space)
 */
 
class Ad_spk_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get ad_spk by id
     */
    function get_ad_spk($id)
    {
        return $this->db->get_where('ad_spk',array('id'=>$id))->row_array();
    }
    function get_all() {
        $this->db->order_by('tanggal_arsip', 'desc');
        return $this->db->get('ad_spk')->result_array();
    }
    /*
     * Get all ad_spk
     */
    function get_all_ad_spk($limit, $start)
    {
        $query = $this->db->get('ad_spk', $limit, $start);
        return $query;
    }
        
    /*
     * function to add new ad_spk
     */
    function add_ad_spk($params)
    {
        $this->db->insert('ad_spk',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update ad_spk
     */
    function update_ad_spk($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('ad_spk',$params);
    }
    
    /*
     * function to delete ad_spk
     */
    function delete_ad_spk($id)
    {
        return $this->db->delete('ad_spk',array('id'=>$id));
    }
}
