<?php
/* 
 * MRI Indonesia
 * Ari Padrian (www.aripadrian.space)
 */
 
class Pengaturan_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get Pengaturan by id
     */
    function get_pengaturan($username)
    {
        return $this->db->get_where('user',array('username'=>$username))->row_array();
    }
        
    /*
     * Get all Pengaturans
     */
    function get_all_pengaturan($limit, $start)
    {
        $this->db->like('level', '2');
        return $this->db->get('user', $limit, $start); 
    }
        
    /*
     * function to add new Pengaturan
     */
    function add_Pengaturan($params)
    {
        $this->db->insert('user',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update Pengaturan
     */
    function update_Pengaturan($username,$params)
    {
        $this->db->where('username',$username);
        return $this->db->update('user',$params);
    }
    
    /*
     * function to delete Pengaturan
     */
    function delete_Pengaturan($id)
    {
        return $this->db->delete('user',array('id'=>$id));
    }
}
