<?php
/* 
 * MRI Indonesia
 * Ari Padrian (www.aripadrian.space)
 */
 
class As_bupati_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get as_bupati by id
     */
    function get_as_bupati($id)
    {
        return $this->db->get_where('as_bupati',array('id'=>$id))->row_array();
    }
    function get_all() {
        $this->db->order_by('tgl_arsip', 'desc');
        return $this->db->get('as_bupati')->result_array();
    } 
    /*
     * Get all as_bupati
     */
    function get_all_as_bupati($limit, $start)
    {
        $query = $this->db->get('as_bupati', $limit, $start);
        return $query;
    }
    /*
     * function to add new as_bupati
     */
    function add_as_bupati($params)
    {
        $this->db->insert('as_bupati',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update as_bupati
     */
    function update_as_bupati($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('as_bupati',$params);
    }
    
    /*
     * function to delete as_bupati
     */
    function delete_as_bupati($id)
    {
        return $this->db->delete('as_bupati',array('id'=>$id));
    }
}
