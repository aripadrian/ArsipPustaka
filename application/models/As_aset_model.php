<?php
/* 
 * MRI Indonesia
 * Ari Padrian (www.aripadrian.space)
 */
 
class As_aset_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get as_aset by id
     */
    function get_as_aset($id)
    {
        return $this->db->get_where('as_aset',array('id'=>$id))->row_array();
    }
    function get_all() {
        $this->db->order_by('tgl_arsip', 'desc');
        return $this->db->get('as_aset')->result_array();
    } 
        
    /*
     * Get all as_aset
     */
    function get_all_as_aset($limit, $start)
    {
        $query = $this->db->get('as_aset', $limit, $start);
        return $query;
    }
        
    /*
     * function to add new as_aset
     */
    function add_as_aset($params)
    {
        $this->db->insert('as_aset',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update as_aset
     */
    function update_as_aset($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('as_aset',$params);
    }
    
    /*
     * function to delete as_aset
     */
    function delete_as_aset($id)
    {
        return $this->db->delete('as_aset',array('id'=>$id));
    }
}
