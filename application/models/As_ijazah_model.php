<?php
/* 
 * MRI Indonesia
 * Ari Padrian (www.aripadrian.space)
 */
 
class As_ijazah_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get as_ijazah by id
     */
    function get_as_ijazah($id)
    {
        return $this->db->get_where('as_ijazah',array('id'=>$id))->row_array();
    }
    function get_all() {
        $this->db->order_by('tgl_arsip', 'desc');
        return $this->db->get('as_ijazah')->result_array();
    } 
    /*
     * Get all as_ijazah
     */
    function get_all_as_ijazah($limit, $start)
    {
        $query = $this->db->get('as_ijazah', $limit, $start);
        return $query;
    }
        
    /*
     * function to add new as_ijazah
     */
    function add_as_ijazah($params)
    {
        $this->db->insert('as_ijazah',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update as_ijazah
     */
    function update_as_ijazah($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('as_ijazah',$params);
    }
    
    /*
     * function to delete as_ijazah
     */
    function delete_as_ijazah($id)
    {
        return $this->db->delete('as_ijazah',array('id'=>$id));
    }
}
