<?php
/* 
 * MRI Indonesia
 * Ari Padrian (www.aripadrian.space)
 */
 
class Ad_sp_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get ad_sp by id
     */
    function get_ad_sp($id)
    {
        return $this->db->get_where('ad_sp',array('id'=>$id))->row_array();
    }
    function get_all() {
        $this->db->order_by('tanggal_arsip', 'desc');
        return $this->db->get('ad_sp')->result_array();
    }
    /*
     * Get all ad_sp
     */
    function get_all_ad_sp($limit, $start)
    {
        $query = $this->db->get('ad_sp', $limit, $start);
        return $query;
    }
        
    /*
     * function to add new ad_sp
     */
    function add_ad_sp($params)
    {
        $this->db->insert('ad_sp',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update ad_sp
     */
    function update_ad_sp($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('ad_sp',$params);
    }
    
    /*
     * function to delete ad_sp
     */
    function delete_ad_sp($id)
    {
        return $this->db->delete('ad_sp',array('id'=>$id));
    }
}
