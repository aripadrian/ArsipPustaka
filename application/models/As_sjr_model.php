<?php
/* 
 * MRI Indonesia
 * Ari Padrian (www.aripadrian.space)
 */
 
class As_sjr_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get as_sjr by id
     */
    function get_as_sjr($id)
    {
        return $this->db->get_where('as_sjr',array('id'=>$id))->row_array();
    }
    function get_all() {
        $this->db->order_by('tgl_arsip', 'desc');
        return $this->db->get('as_sjr')->result_array();
    } 
    /*
     * Get all as_sjr
     */
    function get_all_as_sjr($limit, $start)
    {
        $query = $this->db->get('as_sjr', $limit, $start);
        return $query;
    }
      
    /*
     * function to add new as_sjr
     */
    function add_as_sjr($params)
    {
        $this->db->insert('as_sjr',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update as_sjr
     */
    function update_as_sjr($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('as_sjr',$params);
    }
    
    /*
     * function to delete as_sjr
     */
    function delete_as_sjr($id)
    {
        return $this->db->delete('as_sjr',array('id'=>$id));
    }
}
