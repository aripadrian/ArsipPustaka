<?php
/* 
 * MRI Indonesia
 * Ari Padrian (www.aripadrian.space)
 */
 
class As_pahlawan_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get as_pahlawan by id
     */
    function get_as_pahlawan($id)
    {
        return $this->db->get_where('as_pahlawan',array('id'=>$id))->row_array();
    }
    function get_all() {
        $this->db->order_by('tgl_arsip', 'desc');
        return $this->db->get('as_pahlawan')->result_array();
    } 
    /*
     * Get all as_pahlawan
     */
    function get_all_as_pahlawan($limit, $start)
    {
        $query = $this->db->get('as_pahlawan', $limit, $start);
        return $query;
    }
        
    /*
     * function to add new as_pahlawan
     */
    function add_as_pahlawan($params)
    {
        $this->db->insert('as_pahlawan',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update as_pahlawan
     */
    function update_as_pahlawan($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('as_pahlawan',$params);
    }
    
    /*
     * function to delete as_pahlawan
     */
    function delete_as_pahlawan($id)
    {
        return $this->db->delete('as_pahlawan',array('id'=>$id));
    }
}
