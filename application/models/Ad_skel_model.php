<?php
/* 
 * MRI Indonesia
 * Ari Padrian (www.aripadrian.space)
 */
 
class Ad_skel_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get ad_skel by id
     */
    function get_ad_skel($id)
    {
        return $this->db->get_where('ad_skel',array('id'=>$id))->row_array();
    }
    function get_all() {
        $this->db->order_by('tanggal_arsip', 'desc');
        return $this->db->get('ad_skel')->result_array();
    }
    /*
     * Get all ad_skel
     */
    function get_all_ad_skel($limit, $start)
    {
        $query = $this->db->get('ad_skel', $limit, $start);
        return $query;
    }
        
    /*
     * function to add new ad_skel
     */
    function add_ad_skel($params)
    {
        $this->db->insert('ad_skel',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update ad_skel
     */
    function update_ad_skel($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('ad_skel',$params);
    }
    
    /*
     * function to delete ad_skel
     */
    function delete_ad_skel($id)
    {
        return $this->db->delete('ad_skel',array('id'=>$id));
    }
}
