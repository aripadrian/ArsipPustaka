<?php
/* 
 * MRI Indonesia
 * Ari Padrian (www.aripadrian.space)
 */
 
class As_piagam_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get as_piagam by id
     */
    function get_as_piagam($id)
    {
        return $this->db->get_where('as_piagam',array('id'=>$id))->row_array();
    }
    
    function get_all() {
        $this->db->order_by('tgl_arsip', 'desc');
        return $this->db->get('as_piagam')->result_array();
    } 
    /*
     * Get all as_piagam
     */
    function get_all_as_piagam($limit, $start)
    {
        $query = $this->db->get('as_piagam', $limit, $start);
        return $query;
    }
        
    /*
     * function to add new as_piagam
     */
    function add_as_piagam($params)
    {
        $this->db->insert('as_piagam',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update as_piagam
     */
    function update_as_piagam($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('as_piagam',$params);
    }
    
    /*
     * function to delete as_piagam
     */
    function delete_as_piagam($id)
    {
        return $this->db->delete('as_piagam',array('id'=>$id));
    }
}
