<?php
/* 
 * MRI Indonesia
 * Ari Padrian (www.aripadrian.space)
 */
 
class Ad_dp_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get ad_dp by id
     */
    function get_ad_dp($id)
    {
        return $this->db->get_where('ad_dp',array('id'=>$id))->row_array();
    }
        
    /*
     * Get all ad_dp
     */
    function get_all_ad_dp($limit, $start)
    {
        $this->db->order_by('tanggal_arsip', 'desc');
        $query = $this->db->get('ad_dp', $limit, $start);
        return $query;
    }
    
    function get_all() {
        $this->db->order_by('tanggal_arsip', 'desc');
        return $this->db->get('ad_dp')->result_array();
    }

    /*
     * function to add new ad_dp
     */
    function add_ad_dp($params)
    {
        $this->db->insert('ad_dp',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update ad_dp
     */
    function update_ad_dp($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('ad_dp',$params);
    }
    
    /*
     * function to delete ad_dp
     */
    function delete_ad_dp($id)
    {
        return $this->db->delete('ad_dp',array('id'=>$id));
    }
}
