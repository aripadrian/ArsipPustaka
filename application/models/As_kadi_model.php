<?php
/* 
 * MRI Indonesia
 * Ari Padrian (www.aripadrian.space)
 */
 
class As_kadi_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get as_kadi by id
     */
    function get_as_kadi($id)
    {
        return $this->db->get_where('as_kadis',array('id'=>$id))->row_array();
    }
    function get_all() {
        $this->db->order_by('tgl_arsip', 'desc');
        return $this->db->get('as_kadis')->result_array();
    } 
    /*
     * Get all as_kadis
     */
    function get_all_as_kadi($limit, $start)
    {
        $query = $this->db->get('as_kadis', $limit, $start);
        return $query;
    }
        
    /*
     * function to add new as_kadi
     */
    function add_as_kadi($params)
    {
        $this->db->insert('as_kadis',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update as_kadi
     */
    function update_as_kadi($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('as_kadis',$params);
    }
    
    /*
     * function to delete as_kadi
     */
    function delete_as_kadi($id)
    {
        return $this->db->delete('as_kadis',array('id'=>$id));
    }
}
