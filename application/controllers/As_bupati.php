<?php
/* 
 * MRI Indonesia
 * Ari Padrian (www.aripadrian.space)
 */
 
class as_bupati extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('as_bupati_model');
    } 

    /*
     * Listing of as_bupati
     */
    function index()
    {
        //konfigurasi pagination
        $config['base_url'] = site_url('as_bupati/index'); //site url
        $config['total_rows'] = $this->db->count_all('as_bupati'); //total row
        $config['per_page'] = 10;  //show record per halaman
        $config["uri_segment"] = 3;  // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        // Membuat Style pagination untuk BootStrap v4
      $config['first_link']       = 'Awal';
      $config['last_link']        = 'Terakhir';
      $config['next_link']        = 'Next';
      $config['prev_link']        = 'Prev';
      $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
      $config['full_tag_close']   = '</ul></nav></div>';
      $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
      $config['num_tag_close']    = '</span></li>';
      $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
      $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
      $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
      $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['prev_tagl_close']  = '</span>Next</li>';
      $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
      $config['first_tagl_close'] = '</span></li>';
      $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['last_tagl_close']  = '</span></li>';
      $this->pagination->initialize($config);
      $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
      $data['as_bupati'] = $this->as_bupati_model->get_all_as_bupati($config["per_page"], $data['page']);           
      $data['pagination'] = $this->pagination->create_links();
        $data['_view'] = 'as_bupati/index';
        $this->load->view('layouts/main',$data);
    }
    function cetak() {
        $data['as_bupati'] = $this->as_bupati_model->get_all();
        $this->load->view('as_bupati/cetak',$data);
    }

    /*
     * Adding a new as_bupati
     */
    function add()
    {   
        $tanggal = date("Y-m-d",strtotime($this->input->post('tgl_arsip')));
        $nk = $this->input->post('no');
        $namafile = preg_replace("![^a-z0-9]+!i", "-", $nk);

         //Setting Extensions
         if(isset($_FILES) && count($_FILES) > 0) {
            $path = $_FILES['berkas']['name'];
        } else {
            $path = "FileBelumUpload";
        }
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $namaext = 'arsip/as_bupati/'.$namafile.'.'.$ext;

        // setting konfigurasi upload
        $config['upload_path'] = './arsip/as_bupati';
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['file_name'] = $namafile;
        $config['overwrite'] = 'TRUE';
        // load library upload
        $this->load->library('upload', $config);
        $this->upload->do_upload('berkas');
        
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'no' => $this->input->post('no'),
				'tgl_arsip' => $tanggal,
				'lok_simpan' => $this->input->post('lok_simpan'),
				'petugas' => $this->input->post('petugas'),
				'sifat' => $this->input->post('sifat'),
				'link_file' => $namaext,
                'nama_arsip' => $this->input->post('nama_arsip'),
                'sumber' => $this->input->post('sumber'),
                'deskripsi' => $this->input->post('deskripsi'),
                'kode' => "bupati",
            );
            
            $as_bupati_id = $this->as_bupati_model->add_as_bupati($params);
            helper_log("add", "Menambahkan ".$this->input->post('nama_arsip'));
            redirect('as_bupati/index');
        }
        else
        {            
            $data['_view'] = 'as_bupati/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a as_bupati
     */
    function edit($id)
    {   
       
        $data['as_bupati'] = $this->as_bupati_model->get_as_bupati($id);
        $tanggal = date("Y-m-d",strtotime($this->input->post('tgl_arsip')));
        $nk = $this->input->post('no');
        $namafile = preg_replace("![^a-z0-9]+!i", "-", $nk);

         //Setting Extensions
         if(isset($_FILES) && count($_FILES) > 0) {
            $path = $_FILES['berkas']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $namaext = 'arsip/as_bupati/'.$namafile.'.'.$ext;
        } else { 
            
        };
        // setting konfigurasi upload
        $config['upload_path'] = './arsip/as_bupati';
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['file_name'] = $namafile;
        $config['overwrite'] = 'TRUE';
        // load library upload
        $this->load->library('upload', $config);
        $this->upload->overwrite = true;
        $this->upload->do_upload('berkas');
        if(isset($data['as_bupati']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'no' => $this->input->post('no'),
				'tgl_arsip' => $tanggal,
				'lok_simpan' => $this->input->post('lok_simpan'),
				'petugas' => $this->input->post('petugas'),
				'sifat' => $this->input->post('sifat'),
				'link_file' => $namaext,
                'nama_arsip' => $this->input->post('nama_arsip'),
                'sumber' => $this->input->post('sumber'),
                'deskripsi' => $this->input->post('deskripsi'),
                'kode' => "bupati",
                );

                $this->as_bupati_model->update_as_bupati ($id,$params);                   
                 helper_log("edit", "Mengubah ".$this->input->post('nama_arsip'));  
                    
                redirect('as_bupati/index');
            }
            else
            {
                $data['_view'] = 'as_bupati/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The as_bupati you are trying to edit does not exist.');
    } 

    /*
     * Deleting as_bupati
     */
    function remove($id)
    {
        $as_bupati = $this->as_bupati_model->get_as_bupati($id);

        // check if the as_bupati exists before trying to delete it
        if(isset($as_bupati['id']))
        {
            $file = $as_bupati['link_file'];
            $this->as_bupati_model->delete_as_bupati($id);
            unlink($file);
            helper_log("delete", "Menghapus ".$as_bupati['nama_arsip']);
            redirect('as_bupati/index');
        }
        else
            show_error('Data tidak ada.');
    }
    
}
