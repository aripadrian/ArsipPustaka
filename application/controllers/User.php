<?php
/* 
 * MRI Indonesia
 * Ari Padrian (www.aripadrian.space)
 */
 
class User extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pengaturan_model');
    } 
    function add()
    {   
        $nk = $this->input->post('nama_lengkap');
        //Setting Extensions
        if(isset($_FILES) && count($_FILES) > 0) {
            $path = $_FILES['berkas']['name'];
        } else {
            $path = "FileBelumUpload";
        }
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $namafile = str_replace('/', '_', $nk);
        $namaext = '/arsip/img/'.$namafile.'.'.$ext;
        // setting konfigurasi upload
        $config['upload_path'] = './img';
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['file_name'] = $namafile;
        $config['overwrite'] = 'TRUE';
        // load library upload
        $this->load->library('upload', $config);
        $this->upload->do_upload('berkas');
        
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
                'username' => $this->input->post('username'),
                'level' => 2,
                'jabatan' => $this->input->post('jabatan'),
                'url_foto' => $namaext,
                
                'nama_lengkap' => $this->input->post('nama_lengkap'),
				'password' => md5($this->input->post('password')),
            );
            
            $user_id = $this->Pengaturan_model->add_pengaturan($params);
            redirect('pengaturan/index');
        }
        else
        {            
            $data['_view'] = 'user/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a user
     */
    function edit($id)
    {   
        $nk = $this->input->post('nama_lengkap');
        //Setting Extensions
        if(isset($_FILES) && count($_FILES) > 0) {
            $path = $_FILES['berkas']['name'];
        } else {
            $path = "FileBelumUpload";
        }
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $namafile = str_replace('/', '_', $nk);
        $namaext = '/arsip/img/'.$namafile.'.'.$ext;
        // setting konfigurasi upload
        $config['upload_path'] = './img';
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['file_name'] = $namafile;
        $config['overwrite'] = 'TRUE';
        // load library upload
        $this->load->library('upload', $config);
        $this->upload->do_upload('berkas');
        
        // check if the user exists before trying to edit it
        $data['user'] = $this->Pengaturan_model->get_user($id);
        
        if(isset($data['user']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
                    'username' => $this->input->post('username'),
                    'level' => 2,
                    'jabatan' => $this->input->post('jabatan'),
                    'url_foto' => $namaext,
                    
                    'nama_lengkap' => $this->input->post('nama_lengkap'),
                    'password' => md5($this->input->post('password')),
                );

                $this->Pengaturan_model->update_pengaturan ($id,$params);                             
                redirect('pengaturan/index');
            }
            else
            {
                $data['_view'] = 'user/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The user you are trying to edit does not exist.');
    } 

    /*
     * Deleting user
     */
    function remove($id)
    {
        $user = $this->Pengaturan_model->get_user($id);

        // check if the user exists before trying to delete it
        if(isset($user['id']))
        {
            $this->Pengaturan_model->delete_pengaturan($id);
            redirect('pengaturan/index');
        }
        else
            show_error('The user Data tidak ditemukan.');
    }
    
}
