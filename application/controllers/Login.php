<?php
class Login extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->load->model('login_model');
  }
 
  function index(){
    $this->load->view('V_login');
  }
 
  function auth(){
    $username    = $this->input->post('username',TRUE);
    $password = md5($this->input->post('password',TRUE));
    $validate = $this->login_model->validate($username,$password);
    if($validate->num_rows() > 0){
        $data  = $validate->row_array();
        $id  = $data['id'];
        $username  = $data['username'];
        $nama_lengkap = $data['nama_lengkap'];
        $level = $data['level'];
        $sesdata = array(
            'username'  => $username,
            'id'  => $id,
            'nama_lengkap' => $nama_lengkap,
            'url_foto' => $url_foto,
            'level'     => $level,
            'logged_in' => TRUE
        );
        $this->session->set_userdata($sesdata);
        // access login for admin
        if($level === '1'){
            redirect('admin');
        // access login for penginput
        }else{
            redirect('dashboard');
        }
    }else{
        echo $this->session->set_flashdata('msg','alert(Username atau Password Salah, Coba lagi !)');
        redirect('login');
    }
  }
 
  function logout(){
      $this->session->sess_destroy();
      redirect('login');
  }
 
}