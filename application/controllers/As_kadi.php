<?php
/* 
 * MRI Indonesia
 * Ari Padrian (www.aripadrian.space)
 */
 
class as_kadi extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('as_kadi_model');
    } 

    /*
     * Listing of as_kadi
     */
    function index()
    {
        //konfigurasi pagination
        $config['base_url'] = site_url('as_kadi/index'); //site url
        $config['total_rows'] = $this->db->count_all('as_kadis'); //total row
        $config['per_page'] = 10;  //show record per halaman
        $config["uri_segment"] = 3;  // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        // Membuat Style pagination untuk BootStrap v4
      $config['first_link']       = 'Awal';
      $config['last_link']        = 'Terakhir';
      $config['next_link']        = 'Next';
      $config['prev_link']        = 'Prev';
      $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
      $config['full_tag_close']   = '</ul></nav></div>';
      $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
      $config['num_tag_close']    = '</span></li>';
      $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
      $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
      $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
      $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['prev_tagl_close']  = '</span>Next</li>';
      $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
      $config['first_tagl_close'] = '</span></li>';
      $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['last_tagl_close']  = '</span></li>';
      $this->pagination->initialize($config);
      $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
      $data['as_kadi'] = $this->as_kadi_model->get_all_as_kadi($config["per_page"], $data['page']);           
      $data['pagination'] = $this->pagination->create_links();
        $data['_view'] = 'as_kadi/index';
        $this->load->view('layouts/main',$data);
    }
    function cetak() {
        $data['as_kadi'] = $this->as_kadi_model->get_all();
        $this->load->view('as_kadi/cetak',$data);
    }

    /*
     * Adding a new as_kadi
     */
    function add()
    {   
        $tanggal = date("Y-m-d",strtotime($this->input->post('tgl_arsip')));
        $nk = $this->input->post('no');
        $namafile = preg_replace("![^a-z0-9]+!i", "-", $nk);

         //Setting Extensions
         if(isset($_FILES) && count($_FILES) > 0) {
            $path = $_FILES['berkas']['name'];
        } else {
            $path = "FileBelumUpload";
        }
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $namaext = 'arsip/as_kadi/'.$namafile.'.'.$ext;

        // setting konfigurasi upload
        $config['upload_path'] = './arsip/as_kadi';
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['file_name'] = $namafile;
        $config['overwrite'] = 'TRUE';
        // load library upload
        $this->load->library('upload', $config);
        $this->upload->do_upload('berkas');
        
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'no' => $this->input->post('no'),
				'tgl_arsip' => $tanggal,
				'lok_simpan' => $this->input->post('lok_simpan'),
				'petugas' => $this->input->post('petugas'),
				'sifat' => $this->input->post('sifat'),
				'link_file' => $namaext,
                'nama_arsip' => $this->input->post('nama_arsip'),
                'sumber' => $this->input->post('sumber'),
                'deskripsi' => $this->input->post('deskripsi'),
                'kode' => "ijazah",
            );
            
            $as_kadi_id = $this->as_kadi_model->add_as_kadi($params);
            helper_log("add", "Menambahkan ".$this->input->post('nama_arsip'));
            redirect('as_kadi/index');
        }
        else
        {            
            $data['_view'] = 'as_kadi/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a as_kadi
     */
    function edit($id)
    {   
       
        $data['as_kadi'] = $this->as_kadi_model->get_as_kadi($id);
        $tanggal = date("Y-m-d",strtotime($this->input->post('tgl_arsip')));
        $nk = $this->input->post('no');
        $namafile = preg_replace("![^a-z0-9]+!i", "-", $nk);

         //Setting Extensions
         if(isset($_FILES) && count($_FILES) > 0) {
            $path = $_FILES['berkas']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $namaext = 'arsip/as_kadi/'.$namafile.'.'.$ext;
        } else { 
            
        };
        // setting konfigurasi upload
        $config['upload_path'] = './arsip/as_kadi';
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['file_name'] = $namafile;
        $config['overwrite'] = 'TRUE';
        // load library upload
        $this->load->library('upload', $config);
        $this->upload->overwrite = true;
        $this->upload->do_upload('berkas');
        if(isset($data['as_kadi']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'no' => $this->input->post('no'),
				'tgl_arsip' => $tanggal,
				'lok_simpan' => $this->input->post('lok_simpan'),
				'petugas' => $this->input->post('petugas'),
				'sifat' => $this->input->post('sifat'),
				'link_file' => $namaext,
                'nama_arsip' => $this->input->post('nama_arsip'),
                'sumber' => $this->input->post('sumber'),
                'deskripsi' => $this->input->post('deskripsi'),
                'kode' => "ijazah",
                );

                $this->as_kadi_model->update_as_kadi ($id,$params);                   
                 helper_log("edit", "Mengubah ".$this->input->post('nama_arsip'));  
                    
                redirect('as_kadi/index');
            }
            else
            {
                $data['_view'] = 'as_kadi/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The as_kadi you are trying to edit does not exist.');
    } 

    /*
     * Deleting as_kadi
     */
    function remove($id)
    {
        $as_kadi = $this->as_kadi_model->get_as_kadi($id);

        // check if the as_kadi exists before trying to delete it
        if(isset($as_kadi['id']))
        {
            $file = $as_kadi['link_file'];
            $this->as_kadi_model->delete_as_kadi($id);
            unlink($file);
            helper_log("delete", "Menghapus ".$as_kadi['nama_arsip']);
            redirect('as_kadi/index');
        }
        else
            show_error('Data tidak ada.');
    }
    
}
