<?php
/* 
 * MRI Indonesia
 * Ari Padrian (www.aripadrian.space)
 */
 
class ad_tgh extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('ad_tgh_model');
    } 

    /*
     * Listing of ad_tgh
     */
    function index()
    {
        //konfigurasi pagination
        $config['base_url'] = site_url('ad_tgh/index'); //site url
        $config['total_rows'] = $this->db->count_all('ad_tgh'); //total row
        $config['per_page'] = 10;  //show record per halaman
        $config["uri_segment"] = 3;  // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        // Membuat Style pagination untuk BootStrap v4
      $config['first_link']       = 'Awal';
      $config['last_link']        = 'Terakhir';
      $config['next_link']        = 'Next';
      $config['prev_link']        = 'Prev';
      $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
      $config['full_tag_close']   = '</ul></nav></div>';
      $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
      $config['num_tag_close']    = '</span></li>';
      $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
      $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
      $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
      $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['prev_tagl_close']  = '</span>Next</li>';
      $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
      $config['first_tagl_close'] = '</span></li>';
      $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['last_tagl_close']  = '</span></li>';
      $this->pagination->initialize($config);
      $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
      $data['ad_tgh'] = $this->ad_tgh_model->get_all_ad_tgh($config["per_page"], $data['page']);           
      $data['pagination'] = $this->pagination->create_links();
        $data['_view'] = 'ad_tgh/index';
        $this->load->view('layouts/main',$data);
    }
    function cetak() {
        $data['ad_tgh'] = $this->ad_tgh_model->get_all();
        $this->load->view('ad_tgh/cetak',$data);
    }

    /*
     * Adding a new ad_tgh
     */
    function add()
    {   
        $tanggal = date("Y-m-d",strtotime($this->input->post('tanggal_arsip')));
        $nk = $this->input->post('no_arsip');
        $namafile = preg_replace("![^a-z0-9]+!i", "-", $nk);

         //Setting Extensions
         if(isset($_FILES) && count($_FILES) > 0) {
            $path = $_FILES['berkas']['name'];
        } else {
            $path = "FileBelumUpload";
        }
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $namaext = 'arsip/ad_tgh/'.$namafile.'.'.$ext;

        // setting konfigurasi upload
        $config['upload_path'] = './arsip/ad_tgh';
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['file_name'] = $namafile;
        $config['overwrite'] = 'TRUE';
        // load library upload
        $this->load->library('upload', $config);
        $this->upload->do_upload('berkas');
        
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'no_arsip' => $this->input->post('no_arsip'),
				'tanggal_arsip' => $tanggal,
				'lok_penyimpanan' => $this->input->post('lok_penyimpanan'),
				'petugas' => $this->input->post('petugas'),
				'sifat' => $this->input->post('sifat'),
				'link_file' => $namaext,
				'nama_arsip' => $this->input->post('nama_arsip'),
                'deskripsi' => $this->input->post('deskripsi'),
                'kode' => "tgh",
            );
            
            $ad_tgh_id = $this->ad_tgh_model->add_ad_tgh($params);
            helper_log("add", "Menambahkan ".$this->input->post('nama_arsip'));
            redirect('ad_tgh/index');
        }
        else
        {            
            $data['_view'] = 'ad_tgh/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a ad_tgh
     */
    function edit($id)
    {   
       
        $data['ad_tgh'] = $this->ad_tgh_model->get_ad_tgh($id);
        $tanggal = date("Y-m-d",strtotime($this->input->post('tanggal_arsip')));
        $nk = $this->input->post('no_arsip');
        $namafile = preg_replace("![^a-z0-9]+!i", "-", $nk);

         //Setting Extensions
         if(isset($_FILES) && count($_FILES) > 0) {
            $path = $_FILES['berkas']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $namaext = 'arsip/ad_tgh/'.$namafile.'.'.$ext;
        } else { 
            
        };
        // setting konfigurasi upload
        $config['upload_path'] = './arsip/ad_tgh';
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['file_name'] = $namafile;
        $config['overwrite'] = 'TRUE';
        // load library upload
        $this->load->library('upload', $config);
        $this->upload->overwrite = true;
        $this->upload->do_upload('berkas');
        if(isset($data['ad_tgh']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'no_arsip' => $this->input->post('no_arsip'),
					'tanggal_arsip' =>$tanggal,
					'lok_penyimpanan' => $this->input->post('lok_penyimpanan'),
					'petugas' => $this->input->post('petugas'),
					'sifat' => $this->input->post('sifat'),
					'link_file' => $namaext,
					'nama_arsip' => $this->input->post('nama_arsip'),
                    'deskripsi' => $this->input->post('deskripsi'),
                    'kode' => "tgh",
                );

                $this->ad_tgh_model->update_ad_tgh ($id,$params);                   
                 helper_log("edit", "Mengubah ".$this->input->post('nama_arsip'));  
                    
                redirect('ad_tgh/index');
            }
            else
            {
                $data['_view'] = 'ad_tgh/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The ad_tgh you are trying to edit does not exist.');
    } 

    /*
     * Deleting ad_tgh
     */
    function remove($id)
    {
        $ad_tgh = $this->ad_tgh_model->get_ad_tgh($id);

        // check if the ad_tgh exists before trying to delete it
        if(isset($ad_tgh['id']))
        {
            $file = $ad_tgh['link_file'];
            $this->ad_tgh_model->delete_ad_tgh($id);
            unlink($file);
            helper_log("delete", "Menghapus ".$ad_tgh['nama_arsip']);
            redirect('ad_tgh/index');
        }
        else
            show_error('Data tidak ada.');
    }
    
}
