<?php
/* 
 * MRI Indonesia
 * Ari Padrian (www.aripadrian.space)
 */
 
class ad_skel extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('ad_skel_model');
    } 

    /*
     * Listing of ad_skel
     */
    function index()
    {
        //konfigurasi pagination
        $config['base_url'] = site_url('ad_skel/index'); //site url
        $config['total_rows'] = $this->db->count_all('ad_skel'); //total row
        $config['per_page'] = 10;  //show record per halaman
        $config["uri_segment"] = 3;  // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        // Membuat Style pagination untuk BootStrap v4
      $config['first_link']       = 'Awal';
      $config['last_link']        = 'Terakhir';
      $config['next_link']        = 'Next';
      $config['prev_link']        = 'Prev';
      $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
      $config['full_tag_close']   = '</ul></nav></div>';
      $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
      $config['num_tag_close']    = '</span></li>';
      $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
      $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
      $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
      $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['prev_tagl_close']  = '</span>Next</li>';
      $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
      $config['first_tagl_close'] = '</span></li>';
      $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['last_tagl_close']  = '</span></li>';
      $this->pagination->initialize($config);
      $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
      $data['ad_skel'] = $this->ad_skel_model->get_all_ad_skel($config["per_page"], $data['page']);           
      $data['pagination'] = $this->pagination->create_links();
        $data['_view'] = 'ad_skel/index';
        $this->load->view('layouts/main',$data);
    }
    function cetak() {
        $data['ad_skel'] = $this->ad_skel_model->get_all();
        $this->load->view('ad_skel/cetak',$data);
    }

    /*
     * Adding a new ad_skel
     */
    function add()
    {   
        $tanggal = date("Y-m-d",strtotime($this->input->post('tanggal_arsip')));
        $tanggal_srt = date("Y-m-d",strtotime($this->input->post('tanggal_surat')));
        $nk = $this->input->post('no_arsip');
        $namafile = preg_replace("![^a-z0-9]+!i", "-", $nk);

         //Setting Extensions
         if(isset($_FILES) && count($_FILES) > 0) {
            $path = $_FILES['berkas']['name'];
        } else {
            $path = "FileBelumUpload";
        }
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $namaext = 'arsip/ad_skel/'.$namafile.'.'.$ext;

        // setting konfigurasi upload
        $config['upload_path'] = './arsip/ad_skel';
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['file_name'] = $namafile;
        $config['overwrite'] = 'TRUE';
        // load library upload
        $this->load->library('upload', $config);
        $this->upload->do_upload('berkas');
        
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'no_arsip' => $this->input->post('no_arsip'),
                'tanggal_arsip' => $tanggal,
                'tanggal_surat' => $tanggal_srt,
				'lok_penyimpanan' => $this->input->post('lok_penyimpanan'),
				'petugas' => $this->input->post('petugas'),
				'sifat' => $this->input->post('sifat'),
				'link_file' => $namaext,
				'nama_arsip' => $this->input->post('nama_arsip'),
                'deskripsi' => $this->input->post('deskripsi'),
                'kode' => "skel",
            );
            
            $ad_skel_id = $this->ad_skel_model->add_ad_skel($params);
            helper_log("add", "Menambahkan ".$this->input->post('nama_arsip'));
            redirect('ad_skel/index');
        }
        else
        {            
            $data['_view'] = 'ad_skel/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a ad_skel
     */
    function edit($id)
    {   
       
        $data['ad_skel'] = $this->ad_skel_model->get_ad_skel($id);
        $tanggal = date("Y-m-d",strtotime($this->input->post('tanggal_arsip')));
        $tanggal_srt = date("Y-m-d",strtotime($this->input->post('tanggal_surat')));
        $nk = $this->input->post('no_arsip');
        $namafile = preg_replace("![^a-z0-9]+!i", "-", $nk);

         //Setting Extensions
         if(isset($_FILES) && count($_FILES) > 0) {
            $path = $_FILES['berkas']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $namaext = 'arsip/ad_skel/'.$namafile.'.'.$ext;
        } else { 
            
        };
        // setting konfigurasi upload
        $config['upload_path'] = './arsip/ad_skel';
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['file_name'] = $namafile;
        $config['overwrite'] = 'TRUE';
        // load library upload
        $this->load->library('upload', $config);
        $this->upload->overwrite = true;
        $this->upload->do_upload('berkas');
        if(isset($data['ad_skel']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'no_arsip' => $this->input->post('no_arsip'),
                'tanggal_arsip' => $tanggal,
                'tanggal_surat' => $tanggal_srt,
				'lok_penyimpanan' => $this->input->post('lok_penyimpanan'),
				'petugas' => $this->input->post('petugas'),
				'sifat' => $this->input->post('sifat'),
				'link_file' => $namaext,
				'nama_arsip' => $this->input->post('nama_arsip'),
                'deskripsi' => $this->input->post('deskripsi'),
                'kode' => "skel",
                );

                $this->ad_skel_model->update_ad_skel ($id,$params);                   
                 helper_log("edit", "Mengubah ".$this->input->post('nama_arsip'));  
                    
                redirect('ad_skel/index');
            }
            else
            {
                $data['_view'] = 'ad_skel/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The ad_skel you are trying to edit does not exist.');
    } 

    /*
     * Deleting ad_skel
     */
    function remove($id)
    {
        $ad_skel = $this->ad_skel_model->get_ad_skel($id);

        // check if the ad_skel exists before trying to delete it
        if(isset($ad_skel['id']))
        {
            $file = $ad_skel['link_file'];
            $this->ad_skel_model->delete_ad_skel($id);
            unlink($file);
            helper_log("delete", "Menghapus ".$ad_skel['nama_arsip']);
            redirect('ad_skel/index');
        }
        else
            show_error('Data tidak ada.');
    }
    
}
