<?php
/* 
 * MRI Indonesia
 * Ari Padrian (www.aripadrian.space)
 */
 
class Pengaturan extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pengaturan_model');
        
    }

    function index()
    {
        //konfigurasi pagination
        $config['base_url'] = site_url('pengaturan/index'); //site url
        $config['total_rows'] = $this->db->count_all('user'); //total row
        $config['per_page'] = 10;  //show record per halaman
        $config["uri_segment"] = 3;  // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        // Membuat Style pagination untuk BootStrap v4
      $config['first_link']       = 'Awal';
      $config['last_link']        = 'Terakhir';
      $config['next_link']        = 'Next';
      $config['prev_link']        = 'Prev';
      $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
      $config['full_tag_close']   = '</ul></nav></div>';
      $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
      $config['num_tag_close']    = '</span></li>';
      $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
      $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
      $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
      $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['prev_tagl_close']  = '</span>Next</li>';
      $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
      $config['first_tagl_close'] = '</span></li>';
      $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['last_tagl_close']  = '</span></li>';
      $this->pagination->initialize($config);
      $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
      $data['pengaturan'] = $this->Pengaturan_model->get_all_pengaturan($config["per_page"], $data['page']);           
      $data['pagination'] = $this->pagination->create_links();
        $data['_view'] = 'pengaturan/index';
        $this->load->view('layouts/main-admin',$data);
    }
    
       /*
     * Adding a new ad_dp
     */
    function add()
    {   
        $nk = $this->input->post('username');
        $namafile = preg_replace("![^a-z0-9]+!i", "-", $nk);

         //Setting Extensions
         if(isset($_FILES) && count($_FILES) > 0) {
            $path = $_FILES['berkas']['name'];
        } else {
            $path = "FileBelumUpload";
        }
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $namaext = 'img/'.$namafile.'.'.$ext;

        // setting konfigurasi upload
        $config['upload_path'] = './img/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['file_name'] = $namafile;
        $config['overwrite'] = 'TRUE';
        // load library upload
        $this->load->library('upload', $config);
        $this->upload->do_upload('berkas');
        
        
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
                
				'username' => $this->input->post('username'),
                'level' => "2",
                'password' => md5($this->input->post('password')),
				'nama_lengkap' => $this->input->post('nama_lengkap'),
				'url_foto' => $namaext
            );
            
            $pengaturan_id = $this->Pengaturan_model->add_Pengaturan($params);
            redirect('Pengaturan/index');
        }
        else
        {            
            $data['_view'] = 'Pengaturan/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a Pengaturan
     */
    function edit($username)
    {   
        $data['pengaturan'] = $this->Pengaturan_model->get_pengaturan($username);
        $nk = $this->input->post('username');
        $namafile = preg_replace("![^a-z0-9]+!i", "-", $nk);

         //Setting Extensions
         if(isset($_FILES) && count($_FILES) > 0) {
            $path = $_FILES['berkas']['name'];
        } else {
            $path = "FileBelumUpload";
        }
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $namaext = 'img/'.$namafile.'.'.$ext;

        // setting konfigurasi upload
        $config['upload_path'] = './img/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['file_name'] = $namafile;
        $config['overwrite'] = 'TRUE';
        // load library upload
        $this->load->library('upload', $config);
        $this->upload->do_upload('berkas'); 
       
        
        if(isset($data['pengaturan']['username']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
                    'username' => $this->input->post('username'),
                    'level' => $level,
                    'password' => md5($this->input->post('password')),
                    'nama_lengkap' => $this->input->post('nama_lengkap'),
                    'url_foto' => $namaext
                );
                $this->Pengaturan_model->update_Pengaturan($username,$params);                    
               redirect('login');
            }
            else
            {
                $data['_view'] = 'Pengaturan/edit';
                $this->load->view('layouts/main-atur',$data);
            }
        }
        else
            show_error('The Pengaturan you are trying to edit does not exist.');
    } 

    /*
     * Deleting Pengaturan
     */
    function remove($id)
    {
        $Pengaturan = $this->Pengaturan_model->get_Pengaturan($id);

        // check if the Pengaturan exists before trying to delete it
        if(isset($Pengaturan['id']))
        {
            $this->Pengaturan_model->delete_Pengaturan($id);
            redirect('Pengaturan/index');
        }
        else
            show_error('Data tidak ada.');
    }
}
