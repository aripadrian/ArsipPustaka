<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
	<META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
	<TITLE></TITLE>
	<META NAME="GENERATOR" CONTENT="LibreOffice 4.1.6.2 (Linux)">
	<META NAME="AUTHOR" CONTENT="AP">
	<META NAME="CREATED" CONTENT="20170903;50100000000000">
	<META NAME="CHANGEDBY" CONTENT="AP">
	<META NAME="CHANGED" CONTENT="20170903;51000000000000">
	<META NAME="AppVersion" CONTENT="12.0000">
	<META NAME="DocSecurity" CONTENT="0">
	<META NAME="HyperlinksChanged" CONTENT="false">
	<META NAME="LinksUpToDate" CONTENT="false">
	<META NAME="ScaleCrop" CONTENT="false">
	<META NAME="ShareDoc" CONTENT="false">
	<STYLE TYPE="text/css">
	<!--
		@page { size: 13in 8.5in; margin-left: 0.79in; margin-right: 0.79in; margin-top: 0.5in; margin-bottom: 1in }
		P { margin-bottom: 0.08in; direction: ltr; widows: 2; orphans: 2 }
	-->
	</STYLE>
	<script>
window.print();
</script>
</HEAD>

<BODY LANG="en-US" DIR="LTR">
<P ALIGN=CENTER STYLE="margin-bottom: 0.11in"><FONT SIZE=4><B>Arsip
Surat Masuk<br>Dinas Perpustakaan dan Arsip Kabupaten Kampar</B></FONT></P>
<TABLE WIDTH=1070 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=20>
	<COL WIDTH=123>
	<COL WIDTH=134>
	<COL WIDTH=134>
	<COL WIDTH=131>
	<COL WIDTH=215>
	<COL WIDTH=79>
	<COL WIDTH=122>
	<thead>
	<TR>
		<TD WIDTH=20 STYLE="border: 1px solid #000001; padding: 0in 0.08in">
			<P ALIGN=CENTER>No</P>
		</TD>
		<TD WIDTH=123 STYLE="border: 1px solid #000001; padding: 0in 0.08in">
			<P ALIGN=CENTER>Tanggal Arsip</P>
		</TD>
        <TD WIDTH=123 STYLE="border: 1px solid #000001; padding: 0in 0.08in">
			<P ALIGN=CENTER>Tanggal Surat</P>
		</TD>
		<TD WIDTH=134 STYLE="border: 1px solid #000001; padding: 0in 0.08in">
			<P ALIGN=CENTER>Kode/Nomor</P>
		</TD>
		
		<TD WIDTH=131 STYLE="border: 1px solid #000001; padding: 0in 0.08in">
			<P ALIGN=CENTER>Nama Surat</P>
		</TD>
		<TD WIDTH=215 STYLE="border: 1px solid #000001; padding: 0in 0.08in">
			<P ALIGN=CENTER>Lokasi</P>
		</TD>
		<TD WIDTH=79 STYLE="border: 1px solid #000001; padding: 0in 0.08in">
			<P ALIGN=CENTER>Petugas</P>
		</TD>
		<TD WIDTH=79 STYLE="border: 1px solid #000001; padding: 0in 0.08in">
			<P ALIGN=CENTER>Keterangan</P>
		</TD>
	</TR>
	</thead>
	</tbody>
	<?php
              $no = 1;
              foreach($ad_sma as $a){ ?>
	<TR VALIGN=TOP>
	<TD WIDTH=20 STYLE="border: 1px solid #000001; padding: 0in 0.08in">
			<P><?php echo $no++;?>
			</P>
		</TD>
		<TD WIDTH=20 STYLE="border: 1px solid #000001; padding: 0in 0.08in">
			<P><?php echo date("d-m-Y", strtotime($a['tanggal_arsip']));?>
			</P>
		</TD>
        <TD WIDTH=20 STYLE="border: 1px solid #000001; padding: 0in 0.08in">
			<P><?php echo date("d-m-Y", strtotime($a['tanggal_surat']));?>
			</P>
		</TD>
		<TD WIDTH=123 STYLE="border: 1px solid #000001; padding: 0in 0.08in">
			<P><?php echo $a['no_arsip']; ?></P>
		</TD>
		<TD WIDTH=134 STYLE="border: 1px solid #000001; padding: 0in 0.08in">
			<P><?php echo $a['nama_arsip']; ?>
			</P>
		</TD>
		<TD WIDTH=131 STYLE="border: 1px solid #000001; padding: 0in 0.08in">
			<P><?php echo $a['lok_penyimpanan']; ?></P>
		</TD>
	
		<TD WIDTH=79 STYLE="border: 1px solid #000001; padding: 0in 0.08in">
			<P><?php echo $a['petugas']; ?>
			</P>
		</TD>
		<TD WIDTH=79 STYLE="border: 1px solid #000001; padding: 0in 0.08in">
			<P>
			</P>
		</TD>
		
	</TR>
	  <?php } ?>
</TABLE>