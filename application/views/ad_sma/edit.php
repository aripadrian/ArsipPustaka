<?php echo form_open_multipart('ad_sma/edit/'.$ad_sma['id']); ?>
<script>
document.title = "Ubah Arsip Surat Masuk - Dinas Perpustakaan dan Arsip Kab.Kampar"
</script>

<div class="container mt-3 pl-5 pr-5">
<div class="header">
                      <div class="header-body">
                        <div class="row align-items-center">
                          <div class="col">
                            
                            <!-- Pretitle -->
                            <h6 class="header-pretitle">
                              Edit Arsip
                            </h6>
        
                            <!-- Title -->
                            <h1 class="header-title">
                              Surat Masuk
                            </h1>
        
                          </div>
                         
                        </div> <!-- / .row -->
                      </div>
                    </div>	

<div class="row">
<div class="col-12 col-xl-6">
<div class="card">
	
	<div class="card-body card-block">
	
		   <div class="col-md-12">
			<label for="file-arsip" class="control-label">File Arsip</label>
<div class="form-group">
<input required autocomplete="off" type="file" name="berkas">

</div>

	</div>					
    <div class="col-md-12">
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<label for="no_arsip" class="control-label">Nomor Surat</label>
						<div class="form-group">
							<input autocomplete="off" required type="text" name="no_arsip" onchange="copyIt()" value="<?php echo ($this->input->post('no_arsip') ? $this->input->post('no_arsip') : $ad_sma['no_arsip']); ?>" class="form-control" id="no_arsip" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="tanggal_surat" class="control-label">Tanggal Surat</label>
						<div class="form-group" id="datepicker">
							<input autocomplete="off" required type="text" name="tanggal_surat"  value="<?php echo ($this->input->post('tanggal_surat') ? $this->input->post('tanggal_surat') : date("d-m-Y", strtotime($ad_sma['tanggal_surat']))); ?>" class="form-control" id="datepicker" autocomplete="off" />
						</div>
					</div>
					<div class="col-md-12">
						<label for="nama_arsip" class="control-label">Nama Arsip</label>
						<div class="form-group">
							<input autocomplete="off" required name="nama_arsip" class="form-control" id="nama_arsip" value="<?php echo ($this->input->post('nama_arsip') ? $this->input->post('nama_arsip') : $ad_sma['nama_arsip']); ?>">
						</div>
					</div>
					<div class="col-md-12">
						<label for="deskripsi" class="control-label">Deskripsi Arsip</label>
						<div class="form-group">
							<textarea name="deskripsi" class="form-control" id="deskripsi"><?php echo ($this->input->post('deskripsi') ? $this->input->post('deskripsi') : $ad_sma['deskripsi']); ?></textarea>
						</div>
					</div>
	</div>
	</div>
	</div>
</div>
</div>
</div>
	
<div class="col-12 col-xl-6">
<div class="card">
	
	<div class="card-body card-block">
		<div class="row clearfix">
					<div class="col-md-6">
						<label for="tanggal_arsip" class="control-label">Tanggal Arsip</label>
						<div class="form-group" id="datepicker">
							<input autocomplete="off" required type="text" name="tanggal_arsip"  value="<?php echo ($this->input->post('tanggal_arsip') ? $this->input->post('tanggal_arsip') : date("d-m-Y", strtotime($ad_sma['tanggal_arsip']))); ?>" class="form-control" id="lok_penyimpanan" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="lok_penyimpanan" class="control-label">Lokasi Penyimpanan</label>
						<div class="form-group">
							<input autocomplete="off" required type="text" name="lok_penyimpanan" value="<?php echo ($this->input->post('lok_penyimpanan') ? $this->input->post('lok_penyimpanan') : $ad_sma['lok_penyimpanan']); ?>" class="form-control" id="lok_penyimpanan" />
						</div>
					</div>

					<div class="col-md-6">
						<label for="petugas" class="control-label">Petugas Input</label>
						<div class="form-group">
						<select name="petugas" id="select" class="form-control" >
						<?php 
			$petugas = $this->db->query("SELECT * from user where level like '2%'"); 
			$petugas_array = $petugas->result_array();
              foreach($petugas_array as $a) {?>
			  <option value="<?php echo $a['nama_lengkap']; ?>"
			  <?php if($a['nama_lengkap']==$ad_sma['petugas']) echo 'selected="selected"'; ?>">
			  <?php echo $a['nama_lengkap'] ?>
			  </option>
			  <?php };?>
		</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="sifat" class="control-label">Sifat Arsip</label>
						<div class="form-group">
						<select name="sifat" id="select" class="form-control" >
		
						<?php 
			$sifat_array = array(
				'0'=>'Tertutup',
				'1'=>'Terbuka',
			);
              foreach($sifat_array as $value => $display_text) {?>
			  <option value="<?php echo $value; ?>"
			  <?php if($value==$ad_sma['sifat']) echo 'selected="selected"'; ?>">
			  <?php 
			  if ($value==0) {
				  echo "Terbuka";
			  }else {echo "Tertutup";}
			  ?>
			  </option>
			  <?php };?>
		</select>
		</div>
		<input autocomplete="off" required id="noreg1" type="hidden" name="link_file" value="<?php echo ($this->input->post('link_file') ? $this->input->post('link_file') : $ad_sma['link_file']); ?>">
				</div>
			</div>
					</div>
					<div class="center-block">
			<button type="submit" class="pl-8 pr-8 btn btn-primary mt-2">
            		<i class="fe fe-check"> </i> Simpan
				</button>
				</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>
					</div>


