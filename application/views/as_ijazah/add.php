<?php echo form_open_multipart('as_ijazah/add');?>
<script>
document.title = "Tambah Arsip Ijazah - Dinas Perpustakaan dan Arsip Kab.Kampar"
</script>

<div class="container mt-3 pl-5 pr-5">
<div class="header">
                      <div class="header-body">
                        <div class="row align-items-center">
                          <div class="col">
                            
                            <!-- Pretitle -->
                            <h6 class="header-pretitle">
                              Tambah Arsip
                            </h6>
        
                            <!-- Title -->
                            <h1 class="header-title">
                             Ijazah
                            </h1>
        
                          </div>
                         
                        </div> <!-- / .row -->
                      </div>
                    </div>	

<div class="row">
<div class="col-12 col-xl-6">
<div class="card">
	
	<div class="card-body card-block">
	
		   <div class="col-md-12">
			<label for="file-arsip" class="control-label">File Arsip</label>
<div class="form-group">
<input required autocomplete="off" required  type="file" name="berkas">

</div>
	</div>					
    <div class="col-md-12">
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<label for="no_arsip" class="control-label">Nomor/Kode Arsip</label>
						<div class="form-group">
							<input autocomplete="off" required type="text" name="no" value="<?php echo $this->input->post('no'); ?>" class="form-control" id="no_arsip" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="tanggal_arsip" class="control-label">Tanggal Arsip</label>
						<div class="form-group" id="datepicker">
							<input autocomplete="off" required type="text" name="tanggal_arsip" value="<?php echo $this->input->post('tanggal_arsip'); ?>" class="form-control" id="datepicker" autocomplete="off" />
						</div>
					</div>
					<div class="col-md-12">
						<label for="nama_arsip" class="control-label">Nama Arsip</label>
						<div class="form-group">
							<input autocomplete="off" required name="nama_arsip" class="form-control" id="nama_arsip"><?php echo $this->input->post('nama_arsip'); ?></textarea>
						</div>
					</div>
					<div class="col-md-12">
						<label for="deskripsi" class="control-label">Deskripsi Arsip</label>
						<div class="form-group">
							<textarea name="deskripsi" class="form-control" id="deskripsi"><?php echo $this->input->post('deskripsi'); ?></textarea>
						</div>
					</div>
	</div>
	</div>
	</div>
</div>
</div>
</div>
	
<div class="col-12 col-xl-6">
<div class="card">
	
	<div class="card-body card-block">
					<div class="col-md-12">
						<label for="lok_penyimpanan" class="control-label">Lokasi Penyimpanan</label>
						<div class="form-group">
							<input autocomplete="off" required type="text" name="lok_simpan" value="<?php echo $this->input->post('lok_simpan'); ?>" class="form-control" id="lok_penyimpanan" />
						</div>
					</div>
					<div class="col-md-12">
						<label for="lok_penyimpanan" class="control-label">Penerbit</label>
						<div class="form-group">
							<input autocomplete="off" required type="text" name="sumber" value="<?php echo $this->input->post('sumber'); ?>" class="form-control" id="lok_penyimpanan" />
						</div>
					</div>
					<div class="row ml-1 mr-1">
					<div class="col-md-6">
						<label for="petugas" class="control-label">Petugas Input</label>
						<div class="form-group">
						<select name="petugas" id="select" class="form-control" >
						<option value="">Pilih Petugas</option>
			<?php 
			$petugas = $this->db->query("SELECT * from user where level like '2%'"); 
			$petugas_array = $petugas->result_array();
              foreach($petugas_array as $a) {?>
			  <option value="<?php echo $a['nama_lengkap']; ?>">
			  <?php echo $a['nama_lengkap'] ?>
			  </option>
			  <?php };?>
		</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="sifat" class="control-label">Sifat Arsip</label>
						<div class="form-group">
						<select name="sifat" id="select" class="form-control" >
						<option value="">Pilih Sifat Arsip</option>
			<?php 
			$sifat_values = array(
				'0'=>'Tertutup',
				'1'=>'Terbuka',
			);

			foreach($sifat_values as $value => $display_text)
			{
				$selected = ($value == $this->input->post('sifat')) ? ' selected="selected"' : "";

				echo '<option value="'.$value.'" '.$selected.'>'.$display_text.'</option>';
			} 
			?>
		</select>
		</div>
				</div>
				</div>
			</div>
					</div>
					<div class="center-block">
			<button type="submit" class="pl-8 pr-8 btn btn-primary"  style="">
            		<i class="fe fe-check"> </i> Simpan
				</button>
				</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>
					</div>