<?php
if ($this->session->userdata('level')==1) {
  redirect('admin', 'refresh');
}
?>
<div class="container-fluid mt-3">
<script>
document.title = "Dashboard Sistem Informasi Arsip "  
</script>
                    <div class="row">
                            <div class="col-12 col-xl-8">
                             <div id="carouselExampleSlidesOnly" class="carousel slide card" data-ride="carousel">
                                                    <div class="carousel-inner">
                                                      <div class="carousel-item active">
                                                        <img class="d-block w-100" src="./assets/img/dashboard.jpg" alt="First slide">
                                                      </div>
                                                      <div class="carousel-item">
                                                        <img class="d-block w-100" src="./assets/img/dashboard2.jpg" alt="Second slide">
                                                      </div>
                                                      <div class="carousel-item">
                                                        <img class="d-block w-100" src="./assets/img/dashboard.jpg" alt="Third slide">
                                                      </div>
                                                    </div>
                                                  </div>
                              </div>
                        <div class="col-12 col-xl-4">
            
                        <!-- Projects -->
                        <div class="card">
                          <div class="card-header">
                            <div class="row align-items-center">
                              <div class="col">
                                
                                <!-- Title -->
                                <h4 class="card-header-title">
                                  Riwayat Terakhir
                                </h4>
            
                              </div>
                              <div class="col-auto">
            
                                <!-- Link -->
                                <a href="<?=base_url();?>/riwayat/index" class="small">Lihat Semua</a>
                                
                              </div>
                            </div> <!-- / .row -->
                          </div>
                          <div class="card-body">
                          <?php
                          $this->db->order_by('log_time', 'desc');
                         $log = $this->db->get('tabel_log', 4);
                         foreach($log->result_array() as $a){
                           ?>

                         
            
                            <div class="row align-items-center">
                              <div class="col-auto">
                                
                                  </div>
                              <div class="col ml--2">
            
                                <!-- Title -->
                                <h4 class="card-title mb-1">
                                  <?=$a['log_desc'];?>
                                </h4>
            
                                <!-- Time -->
                                <p class="card-text small text-muted" style="text-transform: capitalize">
                                 Oleh <?=$a['log_user'];?>
                                </p>
                                
                              </div>
                              <div class="col-auto">
                                
                             
                                
                              </div>
                            </div> <!-- / .row -->
            
                            <!-- Divider -->
                            <hr>
                         <?php } ?>
            
                          </div> <!-- / .card-body -->
                        </div> <!-- / .card -->            
            
                      </div>
                     
                  </div>
                  <div class="row">
                      <div class="col-12 col-lg-6 col-xl">
                        <!-- Card -->
                        <div class="card bg-primary">
                          <div class="card-body">
                            <div class="row align-items-center">
                              <div class="col">
                                <!-- Title -->
                                <h6 class="card-title text-uppercase mb-2 text-white">
                                  Total Arsip Dinamis
                                </h6>
                                
                                <!-- Heading -->
                                <span class="h2 mb-0 text-white">
                                <?php
                                  $ad_dp = $this->db->get('ad_dp')->num_rows();
                                  $ad_sk = $this->db->get('ad_sk')->num_rows();
                                  $ad_skel = $this->db->get('ad_skel')->num_rows();
                                  $ad_smas = $this->db->get('ad_smas')->num_rows();
                                  $ad_sp = $this->db->get('ad_sp')->num_rows();
                                  $ad_spk = $this->db->get('ad_spk')->num_rows();
                                  $ad_tgh = $this->db->get('ad_tgh')->num_rows();
                                  $total_ad = $ad_dp + $ad_sk + $ad_skel + $ad_smas + $ad_sp + $ad_spk + $ad_tgh;
                                  echo $total_ad;

                                  
                                  ?>
                                </span>
                    
                              </div>
                              <div class="col-auto">
                                
                                <!-- Icon -->
                                <span class="h2 fe fe-layer text-white mb-0"></span>
            
                              </div>
                            </div> <!-- / .row -->
            
                          </div>
                        </div>
            
                      </div>
                      <div class="col-12 col-lg-6 col-xl">
                        
                        <!-- Card -->
                        <div class="card bg-success">
                          <div class="card-body">
                            <div class="row align-items-center">
                              <div class="col">
            
                                <!-- Title -->
                                <h6 class="card-title text-uppercase text-white mb-2">
                                  Total Arsip Statis
                                </h6>
                                
                                <!-- Heading -->
                                <span class="h2 mb-0 text-white">
                                <?php
                                  $as_aset = $this->db->get('as_aset')->num_rows();
                                  $as_bupati = $this->db->get('as_bupati')->num_rows();
                                  $as_pahlawan = $this->db->get('as_pahlawan')->num_rows();
                                  $as_ijazah = $this->db->get('as_ijazah')->num_rows();
                                  $as_kadis = $this->db->get('as_kadis')->num_rows();
                                  $as_piagam = $this->db->get('as_piagam')->num_rows();
                                  $as_sjr = $this->db->get('as_sjr')->num_rows();
                                  $total_as = $as_aset + $as_pahlawan + $as_bupati + $as_ijazah + $as_kadis + $as_piagam + $as_sjr;
                                  echo $total_as;
                                  ?>
                                </span>
            
                              </div>
                              <div class="col-auto">
                                
                                <!-- Icon -->
                                <span class="h1 fe fe-layer text-white mb-0"></span>
            
                              </div>
                            </div> <!-- / .row -->
            
                          </div>
                        </div>
                          
                      </div>
                      <div class="col-12 col-lg-6 col-xl">
                        
                         <!-- Card -->
                         <div class="card bg-primary">
                                <div class="card-body">
                                  <div class="row align-items-center">
                                    <div class="col">
                  
                                      <!-- Title -->
                                      <h6 class="card-title text-uppercase text-white mb-2">
                                        Arsip dipublikasi
                                      </h6>
                                      
                                      <!-- Heading -->
                                      <span class="h2 mb-0 text-white">
                                      <?php
                                     $ad1 = $this->db->query("SELECT * from ad_dp where sifat like '1%'
                                     UNION SELECT * from ad_sk where sifat like '1%'
                                     UNION SELECT * from ad_sp where sifat like '1%'
                                     UNION SELECT * from ad_spk where sifat like '1%'
                                     UNION SELECT * from ad_tgh where sifat like '1%'");  
                                    $ad1_hasil = $ad1->result_array();
                                   $ad2 = $this->db->query("SELECT * from ad_smas where sifat like '1%'
                                     UNION SELECT * from ad_skel where sifat like '1%'");  
                                    $ad2_hasil = $ad2->result_array();

                                    $as1 = $this->db->query("SELECT * from as_aset where sifat like '1%'
                                     UNION SELECT * from as_bupati where sifat like '1%'
                                     UNION SELECT * from as_kadis where sifat like '1%'
                                     UNION SELECT * from as_pahlawan where sifat like '1%'
                                     UNION SELECT * from as_sjr where sifat like '1%'"); 
                                     $as2 = $this->db->query("SELECT * from as_ijazah where sifat like '1%'");
                                     $as3 = $this->db->query("SELECT * from as_piagam where sifat like '1%'");  
                                     $as1_hasil = $as1->result_array();
                                     $as2_hasil = $as2->result_array();
                                     $as3_hasil = $as3->result_array();
                                    $gabung = array_merge($ad1_hasil,$ad2_hasil,$as1_hasil,$as2_hasil,$as3_hasil);
                                    echo count($gabung);
                                     ?>
                                      </span>
                  
                                    </div>
                                    <div class="col-auto">
                                      
                                      <!-- Icon -->
                                      <span class="h2 fe fe-eye text-white mb-0"></span>
                  
                                    </div>
                                  </div> <!-- / .row -->
                  
                                </div>
                              </div>
                                
                            </div>
              
                      <div class="col-12 col-lg-6 col-xl">
                        
                        <!-- Card -->
                        <div class="card bg-warning">
                                <div class="card-body">
                                  <div class="row align-items-center">
                                    <div class="col">
                  
                                      <!-- Title -->
                                      <h6 class="card-title text-uppercase text-white mb-2">
                                        Arsip tidak dipublikasi
                                      </h6>
                                      
                                      <!-- Heading -->
                                      <span class="h2 mb-0 text-white">
                                      <?php
                                     $ad1 = $this->db->query("SELECT * from ad_dp where sifat like '0%'
                                     UNION SELECT * from ad_sk where sifat like '0%'
                                     UNION SELECT * from ad_sp where sifat like '0%'
                                     UNION SELECT * from ad_spk where sifat like '0%'
                                     UNION SELECT * from ad_tgh where sifat like '0%'");  
                                    $ad1_hasil = $ad1->result_array();
                                   $ad2 = $this->db->query("SELECT * from ad_smas where sifat like '0%'
                                     UNION SELECT * from ad_skel where sifat like '0%'");  
                                    $ad2_hasil = $ad2->result_array();

                                    $as1 = $this->db->query("SELECT * from as_aset where sifat like '0%'
                                     UNION SELECT * from as_bupati where sifat like '0%'
                                     UNION SELECT * from as_kadis where sifat like '0%'
                                     UNION SELECT * from as_pahlawan where sifat like '0%'
                                     UNION SELECT * from as_sjr where sifat like '0%'"); 
                                     $as2 = $this->db->query("SELECT * from as_ijazah where sifat like '0%'");
                                     $as3 = $this->db->query("SELECT * from as_piagam where sifat like '0%'");  
                                     $as1_hasil = $as1->result_array();
                                     $as2_hasil = $as2->result_array();
                                     $as3_hasil = $as3->result_array();


                                     $gabung = array_merge($ad1_hasil,$ad2_hasil,$as1_hasil,$as2_hasil,$as3_hasil);
                                     echo count($gabung);
                                     ?>
                                      </span>
                  
                                    </div>
                                    <div class="col-auto">
                                      
                                      <!-- Icon -->
                                      <span class="h2 fe fe-table text-white mb-0"></span>
                  
                                    </div>
                                  </div> <!-- / .row -->
                  
                                </div>
                              </div>
                                
                            </div>
                  </div>
                   