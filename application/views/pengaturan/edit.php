<?php echo form_open_multipart('pengaturan/edit/'.$pengaturan['username']); ?>
<script>
document.title = "Edit Akun - Dinas Perpustakaan dan Arsip Kab.Kampar"
</script>

<div class="container mt-3 pl-5 pr-5">
<div class="header">
                      <div class="header-body">
                        <div class="row align-items-center">
                          <div class="col">
                            
                            <!-- Pretitle -->
                            <h6 class="header-pretitle">
                              Edit Akun
                            </h6>
        
                            <!-- Title -->
                            <h1 class="header-title">
                             Petugas
                            </h1>
        
                          </div>
                         
                        </div> <!-- / .row -->
                      </div>
                    </div>	

<div class="row ml-3">
<div class="col-12 col-xl-9">
<div class="card">
	
	<div class="card-body card-block">
	
		   <div class="col-md-12">
			<label for="file-arsip" class="control-label">Foto Pengguna</label>
<div class="form-group">
<input required autocomplete="off" required  type="file" name="berkas">

</div>
	</div>					
    <div class="col-md-12">
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-12">
						<label for="nama_lengkap" class="control-label">Nama Lengkap</label>
						<div class="form-group">
							<input autocomplete="off" required type="text" name="nama_lengkap" value="<?php echo ($this->input->post('nama_lengkap') ? $this->input->post('nama_lengkap') : $pengaturan['nama_lengkap']); ?>" class="form-control" id="nama_lengkap" />
						</div>
					</div>
					<div class="col-md-12">
						<label for="username" class="control-label">Nama Pengguna</label>
						<div class="form-group">
							<input autocomplete="off" required type="text" name="username" value="<?php echo ($this->input->post('username') ? $this->input->post('username') : $pengaturan['username']); ?>" class="form-control" autocomplete="off" />
						</div>
					</div>
					<div class="col-md-12">
						<label for="password" class="control-label">Password</label>
						<div class="form-group">
							<input autocomplete="off" type="password" name="password" class="form-control" id="password" value="<?php echo ($this->input->post('password') ? $this->input->post('password') : null); ?>">
							<span class="text-muted">*Jika Anda Menggunakan Password Lama, Kosongkan form ini.</span>
							<script>
							var Val = document.getElementById('password').value;
							if(Val = null) {
								document.getElementById('password').value=<?=$pengaturan['password'];?>;
							}
							</script>
						</div>
					</div>
					<div class="col-md-12">
						<label for="jabatan" class="control-label">Jabatan</label>
						<div class="form-group">
							<textarea name="jabatan" class="form-control" id="jabatan"><?php echo ($this->input->post('jabatan') ? $this->input->post('jabatan') : $pengaturan['jabatan']); ?></textarea>
						</div>
					</div>
	</div>
	</div>
	</div>
</div>
</div>
	
					<div class="center-block">
			<button type="submit" class="pl-8 pr-8 btn btn-primary mb-8"  style="">
            		<i class="fe fe-check"> </i> Simpan
				</button>
				</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>
					</div>