<script>
document.title = "Arsip Surat Disposisi - Dinas Perpustakaan dan Arsip Kab.Kampar"
</script>

<?php
if ($this->session->userdata('level')==2) {
  redirect('/', 'refresh');
}
?>
                   
				   <div class="container-fluid pl-4">
                <div class="row justify-content-center">
                  <div class="col-12">
                    
                    <!-- Header -->
                    <div class="header mt-md-5">
                      <div class="header-body">
                        <div class="row align-items-center">
                          <div class="col">
                            
                            <!-- Pretitle -->
                            <h6 class="header-pretitle">
                              Pengaturan
                            </h6>
        
                            <!-- Title -->
                            <h1 class="header-title">
                             User Level Management
                            </h1>
        
                          </div>
                          <div class="col-auto">
                            <div class="mr-4">
                            <!-- Button -->
                            <a href="<?=base_url();?>pengaturan/add" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Tooltip on top">                             
                              <i class="fe fe-plus pr-2 pl-2"></i>
                              Tambah Pengguna
                            </a>
                          </div>
                            
                          </div>
                        </div> <!-- / .row -->
                      </div>
                    </div>
        
                    <!-- Card -->
                    <div class="card" data-toggle="lists" data-lists-values="[&quot;orders-order&quot;, &quot;orders-product&quot;, &quot;orders-date&quot;, &quot;orders-total&quot;, &quot;orders-status&quot;, &quot;orders-method&quot;]">
                      
                      <div class="table-responsive">
                        <table class="table table-sm table-nowrap card-table">
                          <thead>
                            <tr>
                              <th>
                                <a href="#" class="text-body h5">
                                  No
                                </a>
                              </th>
                              <th>
                                <a href="#" class="text-body h5">
                                  Nama
                                </a>
                              </th>
                              <th>
                                <a href="#" class="text-body h5">
                                  Username
                                </a>
                              </th>
                              <th>
                                <a href="#" class="text-body h5">
                                  Jabatan
                                </a>
                              </th>
                              <th>
                                <a href="#" class="text-body h5">
                                  Level
                                </a>
                              </th>
 
                              <th colspan="2">
                                <a href="#" class="text-body h5 desc">
                                 
                                </a>
                              </th>
                            </tr>
                          </thead>
                          <tbody class="list">
              <?php
              $no = 1;
              foreach($pengaturan->result_array() as $a){ ?>
                    <tr>
						<td><?php echo $no++;?></td>
						<td><?php echo $a['nama_lengkap']; ?></td>
						<td><?php echo $a['username']; ?></td>
            <td><?php echo $a['jabatan']; ?></td>
                        <td><?php $lvl = $a['level'] ;
                        if ($lvl==1) {
                            echo "Administrator";
                        } else { echo "Petugas";}; ?></td>
				
						<td>

								<button type="button" class="btn btn-success" data-toggle="modal"  data-target="#modal<?=$a['id'];?>">
                                  <i class="fe fe-eye" data-toggle="tooltip" data-placement="top" title="Lihat Selengkapnya"></i>
						  </button>
								<!-- Modal <?=$a['id'];?> -->
								<div class="modal fade" id="modal<?=$a['id'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
                                    <div class="modal-content">
          <div class="modal-card card" data-toggle="lists" data-lists-values="[&quot;name&quot;]">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col">

                  <!-- Title -->
                  <h4 class="card-header-title" id="exampleModalCenterTitle">
                    Detail Akun
                  </h4>
              
                </div>
                <div class="col-auto">

                  <!-- Close -->
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
              
                </div>
              </div> <!-- / .row -->
            </div>
            <div class="card-body">
            <div class="row">
            <div class="col-md-8">
            <img style="width:100%;height:100%;" src="<?=base_url();?>/<?php echo $a['url_foto'];?>" alt="<?php echo $a['nama_lengkap'];?>">
            </div>
            <div class="col-md-4">
           <b> Level</b><br><?php $lvl = $a['level'] ;
                        if ($lvl==1) {
                            echo "Administrator";
                        } else { echo "Petugas";}; ?>
                        <br><br>
                        <b>Nama</b>
                        <br><?php echo $a['nama_lengkap'];?>
                        <br><br>
                        <b>Username</b>
                        <br><?php echo $a['username'];?>

            </div>
            </div>

            </div>
          </div>






								<a href="<?php echo site_url('pengaturan/edit/'.$a['username']); ?>" class="btn btn-primary" data-target="#edit<?=$a['id'];?>" data-toggle="tooltip" data-placement="top" title="Ubah Data">
                                  <i class="fe fe-pencil"></i> Edit Akun
                                </a>
									</div>
									</div>
								</div>
								</div>
                                <a href="<?php echo site_url('pengaturan/edit/'.$a['username']); ?>" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Ubah Data">
                                  <i class="fe fe-pencil"></i>
                                </a>
                                <a href="<?php echo site_url('pengaturan/remove/'.$a['id']); ?>" class="btn btn-danger" onclick="return confirm('Apakah anda Yakin Data ini Dihapus?')" data-toggle="tooltip" data-placement="top" title="Hapus Data">
                                  <i class="fe fe-trash"></i>
                                </a>
							  </td>
						  </tr>
						  <?php } ?>
						  </tbody>
						 
                        </table>
                         <!-- Modal <?=$a['id'];?> -->
								<div class="modal fade" id="edit<?=$a['id'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
                                    <div class="modal-content">
          <div class="modal-card card" data-toggle="lists" data-lists-values="[&quot;name&quot;]">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col">

                  <!-- Title -->
                  <h4 class="card-header-title" id="exampleModalCenterTitle">
                    Ubah Akun
                  </h4>
              
                </div>
                <div class="col-auto">

                  <!-- Close -->
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
              
                </div>
              </div> <!-- / .row -->
            </div>
            <div class="card-body">
            <div class="row">
            <div class="col-md-8">
            <img style="width:100%;height:100%;" src="<?=base_url();?>/img/<?php echo $a['url_foto'];?>" alt="<?php echo $a['nama_lengkap'];?>">
            </div>
            <div class="col-md-4">
           <b> Level</b><br><?php $lvl = $a['level'] ;
                        if ($lvl==1) {
                            echo "Administrator";
                        } else { echo "Petugas";}; ?>
                        <br><br>
                        <b>Nama</b>
                        <br><?php echo $a['nama_lengkap'];?>
                        <br><br>
                        <b>Username</b>
                        <br><?php echo $a['username'];?>

            </div>
            </div>
            </div>
          </div>
					  </div>
					  <?php echo $pagination; ?>
                    </div>
        
                  </div>
                </div> <!-- / .row -->
              </div>





