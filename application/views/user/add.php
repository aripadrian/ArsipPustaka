<?php echo form_open_multipart('user/add');?>
<script>
document.title = "Tambah Pengguna"
</script>

<div class="container mt-3 pl-5 pr-5">
<div class="header">
                      <div class="header-body">
                        <div class="row align-items-center">
                          <div class="col">
                            
                            <!-- Pretitle -->
                            <h6 class="header-pretitle">
                              Tambah
                            </h6>
        
                            <!-- Title -->
                            <h1 class="header-title">
                              Pengguna
                            </h1>
        
                          </div>
                         
                        </div> <!-- / .row -->
                      </div>
                    </div>	

<div class="row">
<div class="col-8 col-xl-8">
<div class="card">
	
	<div class="card-body card-block">
	
		   <div class="col-md-12">
			<label for="file-arsip" class="control-label">File Arsip</label>
<div class="form-group">
<input autocomplete="off" required  type="file" name="berkas">

</div>
	</div>
	<div class="col-md-12">
						<label for="nama_lengkap" class="control-label">Nama Lengkap</label>
						<div class="form-group">
							<input autocomplete="off" required type="text" name="nama_lengkap" value="<?php echo $this->input->post('nama_lengkap'); ?>" class="form-control" id="nama_lengkap" />
						</div>
					</div>
					<div class="col-md-12">
						<label for="jabatan" class="control-label">Jabatan</label>
						<div class="form-group">
							<input autocomplete="off" required type="text" name="jabatan" value="<?php echo $this->input->post('jabatan'); ?>" class="form-control" id="jabatan" />
						</div>
					</div>
					<div class="col-md-12">
						<label for="username" class="control-label">Username</label>
						<div class="form-group">
							<input autocomplete="off" required type="text" name="username" value="<?php echo $this->input->post('username'); ?>" class="form-control" id="username" />
						</div>
					</div>
					<div class="col-md-12">
						<label for="password" class="control-label">Password</label>
						<div class="form-group">
							<input autocomplete="off" required type="password" name="password" value="<?php echo $this->input->post('password'); ?>" class="form-control" id="password" />
						</div>
					</div>
	
				<div class="center-block">
			<button type="submit" class="pl-8 pr-8 btn btn-primary"  style="">
            		<i class="fe fe-check"> </i> Simpan
				</button>
				</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>
					</div>