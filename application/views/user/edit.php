<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">User Edit</h3>
            </div>
			<?php echo form_open('user/edit/'.$user['id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="user" class="control-label">User</label>
						<div class="form-group">
							<input required type="text" name="user" value="<?php echo ($this->input->post('user') ? $this->input->post('user') : $user['user']); ?>" class="form-control" id="user" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="pass" class="control-label">Pass</label>
						<div class="form-group">
							<input required type="text" name="pass" value="<?php echo ($this->input->post('pass') ? $this->input->post('pass') : $user['pass']); ?>" class="form-control" id="pass" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>