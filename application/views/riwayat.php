<script>
document.title = "Riwayat Pengguna - Dinas Perpustakaan dan Arsip Kab.Kampar"
</script>

                   
				   <div class="container-fluid pl-4">
                <div class="row justify-content-center">
                  <div class="col-12">
                    
                    <!-- Header -->
                    <div class="header mt-md-5">
                      <div class="header-body">
                        <div class="row align-items-center">
                          <div class="col">
                            
                            <!-- Pretitle -->
                            <h6 class="header-pretitle">
                              Riwayat
                            </h6>
        
                            <!-- Title -->
                            <h1 class="header-title">
                            Riwayat Pengguna
                            </h1>
        
                          </div>
                     
                            
                          </div>
                        </div> <!-- / .row -->
                      </div>
                    </div>
        
                    <!-- Card -->
                    <div class="card col-10" data-toggle="lists" data-lists-values="[&quot;orders-order&quot;, &quot;orders-product&quot;, &quot;orders-date&quot;, &quot;orders-total&quot;, &quot;orders-status&quot;, &quot;orders-method&quot;]">
                      
                      <div class="table-responsive">
                        <table class="table col-12 table-nowrap card-table">
                          <thead>
                            <tr>
                              <th>
                                <a href="#" class="text-body h5">
                                  Waktu
                                </a>
                              </th>
                              <th>
                                <a href="#" class="text-body h5">
                                  Pengguna
                                </a>
                              </th>
                              <th>
                                <a href="#" class="text-body h5">
                                  Deskripsi
                                </a>
                              </th>

                            </tr>
                          </thead>
                          <tbody class="list">
              <?php
              foreach($riwayat->result_array() as $a){ ?>
                    <tr>
						<td><?php echo $a['log_time']; ?></td>
						<td><?php echo $a['log_user']; ?></td>
						<td><?php echo $a['log_desc']; ?></td>
				
		
							  </td>
						  </tr>
						  <?php } ?>
						  </tbody>
						 
                        </table>
					  </div>
					  <?php echo $pagination; ?>
                    </div>
        
                  </div>
                </div> <!-- / .row -->
              </div>





