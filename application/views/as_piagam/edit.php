<?php echo form_open_multipart('as_piagam/edit/'.$as_piagam['id']); ?>
<script>
document.title = "Ubah Arsip Piagam Penghargaan - Dinas Perpustakaan dan Arsip Kab.Kampar"
</script>

<div class="container mt-3 pl-5 pr-5">
<div class="header">
                      <div class="header-body">
                        <div class="row align-items-center">
                          <div class="col">
                            
                            <!-- Pretitle -->
                            <h6 class="header-pretitle">
                              Ubah Arsip
                            </h6>
        
                            <!-- Title -->
                            <h1 class="header-title">
                              Piagam Penghargaan
                            </h1>
        
                          </div>
                         
                        </div> <!-- / .row -->
                      </div>
                    </div>	

<div class="row">
<div class="col-12 col-xl-6">
<div class="card">
	
	<div class="card-body card-block">
	
		   <div class="col-md-12">
			<label for="file-arsip" class="control-label">File Arsip</label>
<div class="form-group">
<input required autocomplete="off" required  type="file" name="berkas">

</div>
	</div>					
    <div class="col-md-12">
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<label for="no_arsip" class="control-label">Nomor/Kode Arsip</label>
						<div class="form-group">
							<input autocomplete="off" required type="text" name="no" value="<?php echo ($this->input->post('no') ? $this->input->post('no') : $as_piagam['no']); ?>" class="form-control" id="no_arsip" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="tanggal_arsip" class="control-label">Tanggal Arsip</label>
						<div class="form-group" id="datepicker">
							<input autocomplete="off" required type="text" name="tgl_arsip" value="<?php echo ($this->input->post('tgl_arsip') ? $this->input->post('tgl_arsip') : date("d-m-Y", strtotime($as_piagam['tgl_arsip']))); ?>" class="form-control" id="datepicker" autocomplete="off" />
						</div>
					</div>
					<div class="col-md-12">
						<label for="nama_arsip" class="control-label">Nama Arsip</label>
						<div class="form-group">
							<input autocomplete="off" required name="nama_arsip" class="form-control" id="nama_arsip" value="<?php echo ($this->input->post('nama_arsip') ? $this->input->post('nama_arsip') : $as_piagam['nama_arsip']); ?>">
						</div>
					</div>
					<div class="col-md-12">
						<label for="deskripsi" class="control-label">Deskripsi Arsip</label>
						<div class="form-group">
							<textarea name="deskripsi" class="form-control" id="deskripsi"><?php echo ($this->input->post('deskripsi') ? $this->input->post('deskripsi') : $as_piagam['deskripsi']); ?></textarea>
						</div>
					</div>
	</div>
	</div>
	</div>
</div>
</div>
</div>
	
<div class="col-12 col-xl-6">
<div class="card">
	
	<div class="card-body card-block">
					<div class="col-md-12">
						<label for="lok_penyimpanan" class="control-label">Lokasi Penyimpanan</label>
						<div class="form-group">
							<input autocomplete="off" required type="text" name="lok_simpan" value="<?php echo ($this->input->post('lok_simpan') ? $this->input->post('lok_simpan') : $as_piagam['lok_simpan']); ?>" class="form-control" id="lok_penyimpanan" />
						</div>
					</div>
					<div class="row ml-1 mr-1">
					<div class="col-md-6">
						<label for="petugas" class="control-label">Petugas Input</label>
						<div class="form-group">
						<select name="petugas" id="select" class="form-control" >
						<?php 
			$petugas = $this->db->query("SELECT * from user where level like '2%'"); 
			$petugas_array = $petugas->result_array();
              foreach($petugas_array as $a) {?>
			  <option value="<?php echo $a['nama_lengkap']; ?>"
			  <?php if($a['nama_lengkap']==$as_piagam['petugas']) echo 'selected="selected"'; ?>">
			  <?php echo $a['nama_lengkap'] ?>
			  </option>
			  <?php };?>
		</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="sifat" class="control-label">Sifat Arsip</label>
						<div class="form-group">
						<select name="sifat" id="select" class="form-control" >
						<?php 
			$sifat_array = array(
				'0'=>'Tertutup',
				'1'=>'Terbuka',
			);
              foreach($sifat_array as $value => $display_text) {?>
			  <option value="<?php echo $value; ?>"
			  <?php if($value==$as_piagam['sifat']) echo 'selected="selected"'; ?>">
			  <?php 
			  if ($value==0) {
				  echo "Terbuka";
			  }else {echo "Tertutup";}
			  ?>
			  </option>
			  <?php };?>
		</select>
		</div>
		</div>
				</div>
			</div>
					</div>
					<div class="center-block">
			<button type="submit" class="pl-8 pr-8 btn btn-primary"  style="">
            		<i class="fe fe-check"> </i> Simpan
				</button>
				</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>
					</div>