<!doctype html>
<html lang="en">
  <head>
  <title></title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow">
  <meta http-equiv="Copyright" content="Dinas Perpustakaan dan Arsip Kampar | MRI Indonesia">
  <meta name="author" content="Pustaka Kampar" >
  <meta http-equiv="imagetoolbar" content="no">
  <meta name="language" content="Indonesia">
  <meta name="revisit-after" content="7">
  <meta name="webcrawlers" content="all">
  <meta name="rating" content="general">
  <meta name="spiders" content="all">

  <link rel="shortcut icon" href="favicon.png" />
  <!-- Required meta tags -->
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/ico/apple-touch-icon.png";?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/ico/favicon-32x32.png";?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/ico/favicon-16x16.png";?>">
    <link rel="mask-icon" href="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/ico/safari-pinned-tab.svg";?>" color="#5bbad5">
    <link rel="shortcut icon" href="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/ico/favicon.ico";?>">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/ico/browserconfig.xml";?>">
    <meta name="theme-color" content="#ffffff">

    <!-- Plugins CSS -->
    <link rel="stylesheet" href="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/libs/flickity/dist/flickity.min.css";?>">
    <link rel="stylesheet" href="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/libs/fullpage.js/dist/jquery.fullpage.min.css";?>">
    <link rel="stylesheet" href="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/libs/font-awesome/css/fontawesome-all.min.css";?>">
    <link rel="stylesheet" href="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/libs/incline-icons/style.min.css";?>">
    <link rel="stylesheet" href="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/libs/highlight.js/styles/codepen-embed.min.css";?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/css/theme.min.css";?>">
    <link rel="stylesheet" href="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/css/custom.css";?>">
    <!-- Google Font CSS -->
    <link href="https://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <!-- AOS -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
     <!-- AOS JS -->
     <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script> 
     <style lang="text/css">
         .cari {
             height: 50px;  
             border-radius: 10px;
             border: solid 2px #212529;
             padding-left: 10px;
             width: 80%;
             }
             
     </style>

  </head>
  <body>
     <!-- Facebook Comments -->
  
      <!-- NAVBAR
    ================================================= -->
    <nav id="navbar1" class="navbar navbar-expand-xl navbar-dark  navbar-togglable  fixed-top">
      <div class="container">

        <!-- Brand -->
        <a class="navbar-brand" href="index.html">
          <a href="/"><img width='220px' src="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/img/logo.png";?>"/></a>

            
           
        </a>
         <!-- Toggler -->
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
         <i class="text-white fas fa-bars"></i>
          </button>
    
          <!-- Collapse -->
          <div class="collapse navbar-collapse" id="navbarCollapse">
  
        
    <!------------------------ MENU ------------------------------>
     <!-- Links -->
     <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="#" id="navbarWelcome" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Beranda
              </a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarLandings" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Profil
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarLandings">
                <a class="dropdown-item " href="#">
                  Struktur Organisasi
                </a>
                <a class="dropdown-item " href="#">
                  Visi Misi
                </a>
                <a class="dropdown-item " href="#">
                  Tujuan
                </a>
              </div>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarPages" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Berita
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarComponents">
                <a class="dropdown-item " href="#">
                  Jurnal
                </a>
                <a class="dropdown-item " href="#">
                  Artikel
                </a>
                <a class="dropdown-item " href="#">
                  Opini
                </a>
                <a class="dropdown-item " href="#">
                  Kegiatan
                </a>
              </div>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarComponents" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Perpustakaan
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarComponents">
                <a class="dropdown-item " href="#">
                  Keanggotaan
                </a>
                <a class="dropdown-item " href="#">
                  OPAC
                </a>
              </div>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarComponents" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Kearsipan
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarComponents">
                <a class="dropdown-item " href="/sis/sikd">
                 SIKD
                </a>
                <a class="dropdown-item " href="/sis/siks">
                SIKS
                </a>
              </div>
            </li>
          </ul>

        </div> <!-- / .navbar-collapse -->
  
      </div> <!-- / .container -->    
    </nav>

  <!------------------------ AKHIR MENU ------------------------------>		

  <!-- DISINI MULAI ISI -->
<div class="container">
<div class="row mt-8">
<div class="col-xl-12 col-md-12">
<div class="bodycari">
<form action="">
<div class="row">
<input type="text" class="cari" placeholder="Cari Arsip"></div>
<div class="row">
<button type="submit" class="btn-cari">Submit</button></div>

</form>
</div>
</div>
</div>
</div>







  <!-- FOOTER -->
  
<footer class="mt-3">
  <div class="pt-5 pb-5">
<div class="container u-space-2">
      <div class="row justify-content-md-between">
        <div class="col-md-4 align-middle">
          <a href="//pustakaarsip.kamparkab.go.id">
        <img height="42px" src="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/img/logo.png";?>" alt="Dinas Perpustakaan dan Arsip Kampar" />
        </a>
        </div>
        <div class="col-md-3">
          <p class="text-white">
            <b>Alamat :</b><br>
          Kantor Perpustakaan dan Arsip <br>Kabupaten Kampar<br>
          Jl. D.I. Panjaitan No. 17<br>
          Bangkinang Kota 28412 <br>
          Tel / Fax: (0762) 323710
          </p>
        </div>
        <div class="col-md-2">
          <span class="text-white m"><b>Link</b></span>
          <ul>
            <a href="/"><li class="text-white li-foot">Beranda</li></a>
            <a href="/"><li class="text-white li-foot">Kontak Kami</li></a>
            <a href="/"><li class="text-white li-foot">Layanan Pustaka</li></a>
            <a href="/"><li class="text-white li-foot">Layanan Arsip</li></a>
            <a href="/"><li class="text-white li-foot">F.A.Q</li></a>
          </ul>
        </div>
        <div class="col-md-3">
          <span class="text-white m"><b>Sosial Media</b></span>
          <ul>
            <a href="/"><li class="text-white li-foot">Facebook</li></a>
            <a href="/"><li class="text-white li-foot">Instagram</li></a>
            <a href="/"><li class="text-white li-foot">Twitter</li></a>
            </ul>
        </div>
      </div>
    </div>
    </div>


</footer>

   <!-- JAVASCRIPT
    ================================================== -->
    <!-- Global JS -->
    <script src="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/libs/jquery/dist/jquery.min.js";?>"></script>
 
    <!-- Plugins JS -->
    <script src="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js";?>"></script>
    <script src="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/libs/flickity/dist/flickity.pkgd.min.js";?>"></script>
    <script src="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/libs/jquery-parallax.js/parallax.min.js";?>"></script>
    <script src="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/libs/waypoints/lib/jquery.waypoints.min.js";?>"></script>
    <script src="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/libs/waypoints/lib/shortcuts/inview.min.js";?>"></script>
    <script src="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/libs/fullpage.js/vendors/scrolloverflow.min.js";?>"></script>
    <script src="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/libs/fullpage.js/dist/jquery.fullpage.min.js";?>"></script>
    <script src="<?php echo "http://pustakaarsip.kamparkab.go.id/layout/mri/assets/libs/highlight.js/highlight.pack.min.js";?>"></script>
 <script>
    $(function() {
  $('#navbar1').addClass('navku');
});
    </script>