<?php if($this->session->userdata('logged_in')) {
$session_data = $this->session->userdata('logged_in');
$data['username'] = $session_data['username'];
  
} else {
  redirect('login', 'refresh');
};
?>
<!doctype html>
<html lang="en">
  <head>
    <title>Sistem Informasi Arsip</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Custom Bootstrapp -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/theme.min.css">
   <!-- Feather Icon -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/feathericon.min.css">
    <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/custom.css">
    <!-- Datepicker -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap-datepicker3.min.css">
   <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
    <script src="<?=base_url();?>assets/js/bootstrap-datepicker.min.js"></script>
     <script>
        $(function () {
  $('[data-toggle="tooltip"]').tooltip()
});
$(function () {
$('#datepicker input').datepicker({
  format: "dd-mm-yyyy"
  })
});
  </script>
    
</head>
  <body>
    <nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-light" id="sidebar">
                <div class="container-fluid">
                  <!-- Toggler -->
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidebarCollapse" aria-controls="sidebarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>
        
                  <!-- Brand -->
                  <a class="navbar-brand" href="<?=base_url();?>">
                    <img src="<?=base_url();?>assets/img/logo.svg" class="navbar-brand-img 
                    mx-auto" alt="Earsip">
                  </a>
        
                         
                  <!-- Collapse -->
                  <div class="collapse navbar-collapse" id="sidebarCollapse">
        
                    <!-- Form -->
                    <form class="mt-4 mb-3 d-md-none">
                      <div class="input-group input-group-rounded input-group-merge">
                        <input type="search" class="form-control form-control-rounded form-control-prepended" placeholder="Search" aria-label="Search">
                        <div class="input-group-prepend">
                          <div class="input-group-text">
                            <span class="fe fe-search"></span>
                          </div>
                        </div>
                      </div>
                    </form>
              
                    <!-- Navigation -->
                    <ul class="navbar-nav">
                      <li class="nav-item">
                        <a class="nav-link" href="<?=base_url();?>">
                          <i class="fe fe-home"></i> Halaman Awal
                        </a>
                        
                      </li>     
                      <li class="nav-item">
                        <a class="nav-link collapsed" href="#sidebarPages" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarPages">
                          <i class="fe fe-book"></i> Arsip Dinamis
                        </a>
                        <div class="collapse" id="sidebarPages">
                          <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                              <a href="<?=base_url();?>ad_sma" class="nav-link ">
                                Surat Masuk
                              </a>
                            </li>
                            <li class="nav-item">
                              <a href="<?=base_url();?>ad_skel" class="nav-link ">
                                Surat Keluar
                              </a>
                            </li>
                            <li class="nav-item">
                              <a href="<?=base_url();?>ad_dp" class="nav-link ">
                                Surat Disposisi
                              </a>
                            </li>
                            <li class="nav-item">
                              <a href="<?=base_url();?>ad_sp" class="nav-link ">
                                Surat Perintah
                              </a>
                            </li>
                            <li class="nav-item">
                              <a href="<?=base_url();?>ad_sk" class="nav-link ">
                                Surat Keputusan
                              </a>
                            </li>
                            <li class="nav-item">
                              <a href="<?=base_url();?>ad_spk" class="nav-link ">
                                Surat Perjanjian Kerjasama
                              </a>
                            </li>
                            <li class="nav-item">
                              <a href="<?=base_url();?>ad_tgh" class="nav-link ">
                               Tagihan
                              </a>
                            </li>
                          </ul>
                        </div>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link collapsed" href="#sidebarAuth" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarAuth">
                          <i class="fe fe-table"></i> Arsip Statis
                        </a>
                        <div class="collapse" id="sidebarAuth">
                          <ul class="nav nav-sm flex-column">
                          <li class="nav-item">
                              <a href="<?=base_url();?>as_bupati" class="nav-link ">
                               Foto Bupati
                              </a>
                            </li>
                            <li class="nav-item">
                              <a href="<?=base_url();?>as_kadi" class="nav-link ">
                               Foto Kepala Dinas
                              </a>
                            </li>
                            <li class="nav-item">
                              <a href="<?=base_url();?>as_pahlawan" class="nav-link ">
                               Foto Pahlawan Kampar
                              </a>
                            </li>
                            <li class="nav-item">
                              <a href="<?=base_url();?>as_sjr" class="nav-link ">
                               Foto Bersejarah Kampar
                              </a>
                            </li>
                            <li class="nav-item">
                              <a href="<?=base_url();?>as_ijazah" class="nav-link ">
                               Ijazah
                              </a>
                            </li>
                            <li class="nav-item">
                              <a href="<?=base_url();?>as_aset" class="nav-link ">
                               Aset Daerah
                              </a>
                            </li>
                            <li class="nav-item">
                              <a href="<?=base_url();?>as_piagam" class="nav-link ">
                              Piagam Penghargaan
                              </a>
                            </li>
                              
                            
                        </div>
                      </li>
                     </ul>
        
                    <!-- Divider -->
                    <hr class="navbar-divider my-3">
                    <ul class="navbar-nav">
                            <li class="nav-item">
                              
                              
                            </li> 
                    </ul>
        
                  </div>
    
              
                    <!-- Push content down -->
                    <div class="mt-auto"></div>                
                    
                    <!-- User (md) -->
                    <div class="navbar-user d-none d-md-flex" id="sidebarUser">
                        &copy 2018 <br>Dispusip Kabupaten Kampar.
                    
        
                    </div>
                    
        
                  </div> <!-- / .navbar-collapse -->
        
                </div>
              </nav>






<div class="pleft">
        <nav class="navbar navbar-expand-lg navbar-light bg-white sticky-top">
                <div class="dropdown">
                        <button type="button" class="btn btn-primary" id="ArsipDinamis" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-left:25px">
                            <i class="fe fe-plus"></i>
                             Tambah Arsip Dinamis</button>
                             <div class="dropdown-menu bg-primary" aria-labelledby="ArsipDinamis">
                                <a class="dropdown-item text-white" href="<?=base_url();?>ad_sma/add">Surat Masuk</a>
                                <a class="dropdown-item text-white" href="<?=base_url();?>ad_skel/add">Surat Keluar</a>
                                <a class="dropdown-item text-white" href="<?=base_url();?>ad_dp/add">Surat Disposisi</a>
                                <a class="dropdown-item text-white" href="<?=base_url();?>ad_sp/add">Surat Perintah</a>
                                <a class="dropdown-item text-white" href="<?=base_url();?>ad_sk/add">Surat Keputusan</a>
                                <a class="dropdown-item text-white" href="<?=base_url();?>ad_spk/add">Surat Perjanjian Kerjasama</a>
                                <a class="dropdown-item text-white" href="<?=base_url();?>ad_tgh/add">Tagihan</a>
                                  </div>
               </div>
                <div class="dropdown">
                                        <button type="button" class="btn btn-success" id="ArsipDinamis" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-left:25px">
                                                <i class="fe fe-plus"></i>
                                                 Tambah Arsip Statis</button>
                                       
                                                <div class="dropdown-menu bg-success" aria-labelledby="ArsipDinamis">
                                                        <a class="dropdown-item text-white" href="<?=base_url();?>as_bupati/add">Foto Bupati</a>
                                                        <a class="dropdown-item text-white" href="<?=base_url();?>as_kadi/add">Foto Kepala Dinas</a>
                                                        <a class="dropdown-item text-white" href="<?=base_url();?>as_pahlawan/add">Foto Pahlawan Kampar</a>
                                                        <a class="dropdown-item text-white" href="<?=base_url();?>as_sjr/add">Foto Bersejarah Kampar</a>
                                                        <a class="dropdown-item text-white" href="<?=base_url();?>as_ijazah/add">Ijazah</a>
                                                        <a class="dropdown-item text-white" href="<?=base_url();?>as_aset/add">Aset Daerah</a>
                                                        <a class="dropdown-item text-white" href="<?=base_url();?>as_piagam/add">Piagam Penghargaan</a>
                                                          </div>
                                     
                </div>

               
                
                <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex mr-5">
                <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" data-toggle="dropdown" aria-expanded="false">
          <span class="d-inline-block d-md-none">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-settings align-middle"><circle cx="12" cy="12" r="3"></circle><path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path></svg>
          </span>
          <span class="d-none d-sm-inline-block">
            <img onerror="this.src='<?=base_url();?>/img/user.png'" src="<?=base_url();?>/<?php echo $this->session->userdata('url_foto');?>" class="avatar img-fluid rounded-circle mr-1"> <span class="text-dark"><?php echo $this->session->userdata('nama_lengkap');?> </span>
          </span>
        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="<?=base_url();?>pengaturan/edit/<?php echo $this->session->userdata('username');?> ">Pengaturan</a>
                            <a class="dropdown-item" href="<?=base_url();?>panduan">Panduan</a>
                            <a class="dropdown-item" href="<?=base_url();?>dashboard/logout">Keluar</a>
                        </div>
                    </li>
    </ul>
              </nav>           
 <!-- MULAI KONTEN -->
 <?php   if(isset($_view) && $_view)
    $this->load->view($_view);
    ?>  
    <!-- AKHIR KONTEN -->
    </body>
   
    </html>