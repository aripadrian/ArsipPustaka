<?php echo validation_errors(); ?>
<html>
    <head>
        <title>Sistem Informasi Arsip</title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
<style>
@import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");
html body {
  overflow: hidden;
  font-family: 'Rubik', sans-serif;
}
.login-block{
  background: #1488CC;  /* fallback for old browsers */
background: -webkit-linear-gradient(to right, #2B32B2, #1488CC);  /* Chrome 10-25, Safari 5.1-6 */
background: linear-gradient(to right, #2B32B2, #1488CC); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
float:left;
width:100%;
padding : 50px 0;
height: 800px;
}
.banner-sec{background:url("./assets/img/bg.svg")  no-repeat left bottom; background-size:cover; min-height:500px; border-radius: 0 10px 10px 0; padding:0;}
.container{background:#fff; border-radius: 10px; box-shadow:15px 20px 0px rgba(0,0,0,0.1);}
.carousel-inner{border-radius:0 10px 10px 0;}
.carousel-caption{text-align:left; left:5%;}
.login-sec{padding: 50px 30px; position:relative;}
.login-sec .copy-text{position:absolute; width:80%; bottom:20px; font-size:13px; text-align:center;}
.login-sec .copy-text i{color:#007bff;}
.login-sec .copy-text a{color:#007bff;}
.login-sec h2{margin-bottom:30px; font-weight:800; font-size:30px; color: #007bff;}
.login-sec h2:after{content:" "; width:100px; height:5px; background:#007bff; display:block; margin-top:20px; border-radius:3px; margin-left:auto;margin-right:auto}
.btn-login{background: #007bff; color:#fff; font-weight:600;}
.banner-text{width:70%; position:absolute; bottom:40px; padding-left:20px;}
.banner-text h2{color:#fff; font-weight:600;}
.banner-text h2:after{content:" "; width:100px; height:5px; background:#FFF; display:block; margin-top:20px; border-radius:3px;}
.banner-text p{color:#fff;}
</style>
<!------ Include the above in your HEAD tag ---------->
</head>
<body>
<section class="login-block">
    <div class="container">
	<div class="row">
		<div class="col-md-4 login-sec">
		    <h2 class="text-center">Silahkan <br>Login Dahulu.</h2>
            <form class="login-form" method="post" action="<?php echo site_url('login/auth');?>">
  <div class="form-group">
    <label for="exampleInputEmail1" class="">Username</label>
    <input type="text" class="form-control validitas" name="username" placeholder="">
    
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1" class="">Password</label>
    <input type="password" class="form-control validate" name="password" id="password">
  </div>
  
  
    <div class="form-check">
    
    <button type="submit" class="btn btn-login float-right">Login</button>
  </div>
  
</form>
<div class="copy-text">&copy;Dinas Perpustakaan dan Arsip Kampar</a></div>
		</div>
		<div class="col-md-8 banner-sec">
           
            <div class="carousel-inner" role="listbox">
    <div class="carousel-item">
      <img class="d-block img-fluid" src="./assets/img/dashboard2.jpg" alt="First slide">
    </div>
  
  </div>
            </div>	   
		    
		</div>
	</div>
</div>
</section>
