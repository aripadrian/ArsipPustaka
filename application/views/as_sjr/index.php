<script>
document.title = "Arsip Sejarah Kampar - Dinas Perpustakaan dan Arsip Kab.Kampar"
</script>

                   
				   <div class="container-fluid pl-4">
                <div class="row justify-content-center">
                  <div class="col-12">
                    
                    <!-- Header -->
                    <div class="header mt-md-5">
                      <div class="header-body">
                        <div class="row align-items-center">
                          <div class="col">
                            
                            <!-- Pretitle -->
                            <h6 class="header-pretitle">
                              Arsip
                            </h6>
        
                            <!-- Title -->
                            <h1 class="header-title">
                            Sejarah Kampar
                            </h1>
        
                          </div>
                          <div class="col-auto">
                            <div class="mr-4">
                            <!-- Button -->
                            <a href="<?=base_url();?>as_sjr/add" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Tooltip on top">                             
                              <i class="fe fe-plus pr-2 pl-2"></i>
                              Tambah Arsip
                            </a>
                            <a href="<?=base_url();?>as_sjr/cetak" class="btn btn-success ml-2">
                              <i class="fe fe-print text-white pr-2 pl-2"></i>
                              Cetak Rekap
                            </a>
                          </div>
                            
                          </div>
                        </div> <!-- / .row -->
                      </div>
                    </div>
        
                    <!-- Card -->
                    <div class="card" data-toggle="lists" data-lists-values="[&quot;orders-order&quot;, &quot;orders-product&quot;, &quot;orders-date&quot;, &quot;orders-total&quot;, &quot;orders-status&quot;, &quot;orders-method&quot;]">
                      
                      <div class="table-responsive">
                        <table class="table table-sm table-nowrap card-table">
                          <thead>
                            <tr>
                              <th>
                                <a href="#" class="text-body h5">
                                  No
                                </a>
                              </th>
                              <th>
                                <a href="#" class="text-body h5">
                                  Kode
                                </a>
                              </th>
                              <th>
                                <a href="#" class="text-body h5">
                                  Nama Surat
                                </a>
                              </th>
                              <th>
                                <a href="#" class="text-body h5">
                                  Tanggal Arsip
                                </a>
                              </th>
                              <th>
                                <a href="#" class="text-body h5">
                                  Lokasi
                                </a>
                              </th>
 
                              <th colspan="2">
                                <a href="#" class="text-body h5 desc">
                                 
                                </a>
                              </th>
                            </tr>
                          </thead>
                          <tbody class="list">
              <?php
              $no = 1;
              foreach($as_sjr->result_array() as $a){ ?>
                    <tr>
						<td><?php echo $no++;?></td>
						<td><?php echo $a['no']; ?></td>
						<td><?php echo $a['nama_arsip']; ?></td>
            <td><?php echo date("d-m-Y", strtotime($a['tgl_arsip']));?></td>
						<td><?php echo $a['lok_simpan']; ?></td>
				
						<td>

								<button type="button" class="btn btn-success" data-toggle="modal"  data-target="#modal<?=$a['id'];?>">
                                  <i class="fe fe-eye" data-toggle="tooltip" data-placement="top" title="Lihat Selengkapnya"></i>
						  </button>
								<!-- Modal <?=$a['id'];?> -->
								<div class="modal fade" id="modal<?=$a['id'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">


<div class="modal-content">
          <div class="modal-card card" data-toggle="lists" data-lists-values="[&quot;name&quot;]">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col">

                  <!-- Title -->
                  <h4 class="card-header-title" id="exampleModalCenterTitle">
                    Detail Arsip
                  </h4>
              
                </div>
                <div class="col-auto">

                  <!-- Close -->
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
              
                </div>
              </div> <!-- / .row -->
            </div>
            <div class="card-body">

              <!-- List group -->
              <ul class="list-group list-group-flush list my--3">
			  <li class="list-group-item px-0">
              
                  <div class="row">
                   
                    <div class="col-4">
                      <h4 class="mb-1 name">
					 Nomor / Kode Arsip
                      </h4>

					</div>
					<div class="col-4 ">
                      <h4>
					 : <?php echo $a['no']; ?>
                      </h4>
                    </div>
                  </div> <!-- / .row -->
				</li>
				<li class="list-group-item px-0">
              
			  <div class="row">
			   
				<div class="col-4">
				  <h4 class="mb-1 name">
				 Nama Arsip
				  </h4>

				</div>
				<div class="col-4 ">
				  <h4>
				 : <?php echo $a['nama_arsip']; ?>
				  </h4>
				</div>
			  </div> <!-- / .row -->
			</li>
			<li class="list-group-item px-0">
              
			  <div class="row">
			   
				<div class="col-4">
				  <h4 class="mb-1 name">
				 Deskripsi Arsip
				  </h4>

				</div>
				<div class="col-4 ">
				  <h4>
				 : <?php echo $a['deskripsi']; ?>
				  </h4>
				</div>
			  </div> <!-- / .row -->
			</li>
			<li class="list-group-item px-0">
              
			  <div class="row">
			   
				<div class="col-4">
				  <h4 class="mb-1 name">
				 Tanggal diarsipkan
				  </h4>
				</div>
				<div class="col-4 ">
				  <h4>
				 : <?php echo date("d-m-Y", strtotime($a['tgl_arsip']));?>
				  </h4>
				</div>
			  </div> <!-- / .row -->
			</li>
			
			<li class="list-group-item px-0">
              
			  <div class="row">
			   
				<div class="col-4">
				  <h4 class="mb-1 name">
				 Diarsipkan Oleh
				  </h4>

				</div>
				<div class="col-4 ">
				  <h4>
				 : <?php echo $a['petugas']; ?>
				  </h4>
				</div>
			  </div> <!-- / .row -->
			</li>
			<li class="list-group-item px-0">
              
			  <div class="row">
			   
				<div class="col-4">
				  <h4 class="mb-1 name">
				 Lokasi Arsip
				  </h4>

				</div>
				<div class="col-4 ">
				  <h4>
				 : <?php echo $a['lok_simpan']; ?>
				  </h4>
				</div>
			  </div> <!-- / .row -->
			</li>
				</ul>

            </div>
          </div>
        </div>

									<div class="modal-footer">
										<?php
										$n = $a['sifat'];
										if ($n==0) {
											echo "<a class='btn btn-danger text-white'>
											<i class='fe fe-eye'></i> Tidak di Publikasi
										  </a>";
										} else {
											echo "<a class='btn btn-warning	 text-white'>
											<i class='fe fe-eye'></i> Arsip di Publikasi
										  </a>";
										};
										?>
									
									<a href="<?=base_url().$a['link_file']?>" target="new_tab" class="btn btn-success">
                                  <i class="fe fe-eye" data-toggle="tooltip" data-placement="top" title="Lihat Selengkapnya"></i> Lihat File
								</a>
								<a href="<?php echo site_url('as_sjr/edit/'.$a['id']); ?>" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Ubah Data">
                                  <i class="fe fe-pencil"></i> Edit Arsip
                                </a>
									</div>
									</div>
								</div>
								</div>
                                <a href="<?php echo site_url('as_sjr/edit/'.$a['id']); ?>" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Ubah Data">
                                  <i class="fe fe-pencil"></i>
                                </a>
                                <a href="<?php echo site_url('as_sjr/remove/'.$a['id']); ?>" class="btn btn-danger" onclick="return confirm('Apakah anda Yakin Data ini Dihapus?')" data-toggle="tooltip" data-placement="top" title="Hapus Data">
                                  <i class="fe fe-trash"></i>
                                </a>
							  </td>
						  </tr>
						  <?php } ?>
						  </tbody>
						 
                        </table>
					  </div>
					  <?php echo $pagination; ?>
                    </div>
        
                  </div>
                </div> <!-- / .row -->
              </div>





